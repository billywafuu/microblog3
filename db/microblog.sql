-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2020 at 07:31 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` varchar(16) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `activation_token` varchar(16) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `activation_token`, `created`) VALUES
('305T76A79Z0LQF4J', 66, '65T3YE296USPKINB', '2020-12-16 09:17:27'),
('42801897KK4K7EK9', 54, 'N1KMAZI5R241L601', '2020-12-14 05:15:30'),
('FRQV96OF5W0DG6ZV', 63, '9G7L0UBTJ4II49A4', '2020-12-16 09:10:53'),
('L090SP242FUM35Q3', 62, '1EO3TM83ZFV9G4S3', '2020-12-16 09:10:08'),
('U2RS25AGCV9TLXWV', 64, '0LBAJC4763JIUZ99', '2020-12-16 09:11:10');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `comment` text NOT NULL,
  `created` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted` smallint(6) DEFAULT 0 COMMENT '# 0 - Not deleted # 1 - Deleted',
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `status` smallint(6) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `comment`, `created`, `deleted_at`, `deleted`, `modified`, `status`) VALUES
(43, 34, 38, 'First test', '2020-12-14 16:32:26', NULL, 0, '2020-12-14 08:32:26', 1),
(44, 34, 55, 'second test', '2020-12-15 10:09:06', NULL, 0, '2020-12-15 02:09:06', 1),
(45, 34, 55, 'nice', '2020-12-15 10:10:32', NULL, 0, '2020-12-15 02:10:32', 1),
(46, 34, 38, 'test', '2020-12-18 10:02:00', NULL, 0, '2020-12-18 02:02:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

CREATE TABLE `follows` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `following_id` bigint(20) NOT NULL,
  `date_followed` datetime NOT NULL DEFAULT current_timestamp(),
  `date_unfollowed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`id`, `user_id`, `following_id`, `date_followed`, `date_unfollowed`) VALUES
(5, 45, 45, '2020-11-25 14:27:07', NULL),
(8, 47, 47, '2020-11-25 14:27:07', NULL),
(9, 47, 45, '2020-11-25 14:27:07', NULL),
(10, 47, 38, '2020-11-25 14:27:07', NULL),
(11, 38, 38, '2020-11-25 14:27:07', NULL),
(17, 38, 47, '2020-12-10 07:07:38', '2020-12-10 15:21:28'),
(24, 38, 47, '2020-12-10 07:21:37', '2020-12-10 15:21:42'),
(25, 38, 47, '2020-12-10 07:25:46', '2020-12-10 15:25:57'),
(26, 55, 55, '2020-12-10 07:25:46', '2020-12-10 15:25:57');

-- --------------------------------------------------------

--
-- Table structure for table `forgot_passwords`
--

CREATE TABLE `forgot_passwords` (
  `id` varchar(16) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `reset_token` varchar(16) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forgot_passwords`
--

INSERT INTO `forgot_passwords` (`id`, `user_id`, `reset_token`, `created`) VALUES
('6855V1L6XDU44PY2', 38, '87X5EEHXNOI80872', '2020-12-15 16:38:33'),
('6SO3IZT0JG7I2W00', 68, '738HR992DO2MI0CC', '2020-12-22 16:38:35');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `post_id` int(11) NOT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT 0 COMMENT '0 - Not deleted, 1 - Deleted',
  `deleted_at` datetime DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `deleted`, `deleted_at`, `created`) VALUES
(4, 38, 12, 1, '2020-12-04 08:41:38', '2020-12-04 08:41:31'),
(5, 38, 12, 1, '2020-12-04 08:41:52', '2020-12-04 08:41:46'),
(6, 38, 31, 1, '2020-12-14 13:50:05', '2020-12-14 13:49:57'),
(7, 38, 31, 1, '2020-12-14 13:50:09', '2020-12-14 13:50:08'),
(8, 38, 33, 0, NULL, '2020-12-14 14:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT 0,
  `title` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `image_file_type` varchar(4) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `post_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `created`, `modified`, `deleted_at`, `deleted`, `title`, `body`, `image_file_type`, `status`, `post_id`) VALUES
(9, 1, '2020-11-25 13:57:56', '2020-11-25 13:57:56', NULL, 0, 'test', 'test', '', 1, NULL),
(12, 38, '2020-12-03 03:28:20', '0000-00-00 00:00:00', NULL, 0, 'AK and M4', 'test', 'jpg', 1, NULL),
(13, 38, '2020-12-03 03:42:16', '0000-00-00 00:00:00', NULL, 0, 'test', 'asd', '', 1, 123),
(15, 45, '2020-12-09 13:17:13', '0000-00-00 00:00:00', NULL, 0, 'EC 620', 'SOPMOD Block II', 'jpg', 1, 13),
(16, 38, '2020-12-04 04:29:48', '0000-00-00 00:00:00', NULL, 0, 'test 2', 'test', 'jpg', 1, NULL),
(17, 38, '2020-12-04 08:53:29', '0000-00-00 00:00:00', NULL, 0, '@retweet', 'me want', '', 1, 12),
(18, 47, '2020-12-09 03:01:55', '0000-00-00 00:00:00', NULL, 0, 'Juan - Test 1', 'This is a test post.', '', 1, NULL),
(19, 47, '2020-12-09 03:02:09', '0000-00-00 00:00:00', NULL, 0, 'Juan - Test 2', 'This is the second test post.', '', 1, NULL),
(20, 47, '2020-12-09 05:52:15', '0000-00-00 00:00:00', NULL, 0, 'zxczxc', 'zxczxc', '', 1, NULL),
(21, 47, '2020-12-09 05:53:23', '0000-00-00 00:00:00', NULL, 0, 'test 3', 'test 3', '', 1, NULL),
(22, 38, '2020-12-09 05:56:10', '0000-00-00 00:00:00', NULL, 0, 'boss bill', 'boss bill\r\n', '', 1, NULL),
(23, 38, '2020-12-09 14:00:58', '0000-00-00 00:00:00', NULL, 0, 'test test', 'tesdasdadasd', '', 1, NULL),
(24, 38, '2020-12-09 14:03:03', '0000-00-00 00:00:00', NULL, 0, 'hi bill', '', '', 1, NULL),
(25, 38, '2020-12-09 14:40:28', '0000-00-00 00:00:00', NULL, 0, 'Hello World', 'asdasd', '', 1, NULL),
(26, 38, '2020-12-09 14:41:15', '0000-00-00 00:00:00', NULL, 0, 'yo', 'wan', '', 1, NULL),
(27, 47, '2020-12-09 14:42:42', '0000-00-00 00:00:00', NULL, 0, 'John Wick Set', 'My collection of John Wick\'s guns.', 'jpg', 1, NULL),
(28, 38, '2020-12-14 13:24:13', '0000-00-00 00:00:00', NULL, 0, 'Hello World', '', '', 1, NULL),
(29, 38, '2020-12-14 13:26:05', '0000-00-00 00:00:00', NULL, 0, 'Hello World', 'Hello World', '', 1, NULL),
(30, 38, '2020-12-14 13:27:01', '0000-00-00 00:00:00', NULL, 0, 'Disappointed Muslim', '', 'jpg', 1, NULL),
(31, 38, '2020-12-14 13:46:16', '0000-00-00 00:00:00', '2020-12-14 14:00:13', 1, '<script>alert(\"nice\")</script>', '<script>alert(\"nice\")</script>', '', 1, NULL),
(32, 38, '2020-12-14 13:50:18', '0000-00-00 00:00:00', '2020-12-14 14:00:30', 1, '@retweeted', 'hahahahahaha', '', 1, 31),
(33, 38, '2020-12-14 14:08:24', '0000-00-00 00:00:00', NULL, 0, '<script>alert(\"nice\")</script>', '<script>alert(\"nice\")</script>', '', 1, NULL),
(34, 38, '2020-12-14 16:32:18', '0000-00-00 00:00:00', NULL, 0, 'Comment Test', 'This is a comment testing thread.', '', 1, NULL),
(35, 55, '2020-12-18 14:56:12', '0000-00-00 00:00:00', '2020-12-18 15:04:25', 1, 'Pedal of the Year', 'Get it on Ebay.', 'jpg', 1, NULL),
(36, 55, '2020-12-18 15:05:02', '0000-00-00 00:00:00', NULL, 0, 'The Rock', 'The cock too', 'jpg', 1, NULL),
(37, 68, '2020-12-22 16:51:21', '0000-00-00 00:00:00', '2020-12-22 17:20:09', 1, 'USER MANUAL ', 'The sun is shiny outside ', 'jpg', 1, NULL),
(38, 55, '2020-12-23 13:45:23', '2020-12-23 05:45:23', NULL, 0, 'test', 'test', '', 1, NULL),
(39, 55, '2020-12-23 13:47:28', '2020-12-23 05:47:28', NULL, 0, 'test', 'test', '', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `birthdate` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT 0 COMMENT '0 - Not deleted, 1 - deleted',
  `deleted_at` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT 0 COMMENT '0 - Activation required, 1 - Active, 2 - Banned',
  `image_file_type` varchar(4) NOT NULL DEFAULT 'png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email_address`, `password`, `birthdate`, `created`, `modified`, `deleted`, `deleted_at`, `status`, `image_file_type`) VALUES
(38, 'Bill Dwight', 'コリンコ', 'Ijiran', 'dwight.ijiran@gmail.com', '$2y$10$9eNRmBDrBr7.xzqcb1x3wOsHKTOLlMUuKY9SJ5dbOVuVR.YmJ99te', '1997-01-01', '0000-00-00 00:00:00', NULL, 0, NULL, 1, ''),
(47, 'Juan Cruz', 'Dela', 'Cruz', 'juandelacruz@gmail.com', '$2y$10$ZB9e4t3kzjB9Ef/iRZXMhOrOznd1bbzVzVRIaMSsRdmzpBuCD8Vx2', '1997-01-01', '0000-00-00 00:00:00', NULL, 0, NULL, 1, 'png'),
(48, 'Boss', '', 'Bill', 'ijiran197@gmail.com', '$2y$10$29SW4Iv50JtZDEC8O8ArcOcB/SSnHuW6zVugRp/HQ4kRu5XLiXarK', '1997-01-01', '0000-00-00 00:00:00', NULL, 0, NULL, 0, 'png'),
(55, 'Fuloze Roy', '', 'Reyes', 'puhutel@boximail.com', '$2y$10$UMfOjYvf3k4ZEUTfC.ahQu7hZohgeF7uDwr0xzEqpm9MxdyNIW58W', '1998-02-12', '0000-00-00 00:00:00', NULL, 0, NULL, 1, 'jpg'),
(67, 'Mikero', '', 'Blag', 'microblog@clrmail.com', '$2y$10$QI4pR2LJumJRz9BYP2Xu0.8bAQCg5Ap2j8asGC6o/T081Pq9W4YQK', '1999-05-05', '2020-12-16 17:20:02', NULL, 0, NULL, 1, 'png'),
(68, 'Melody ', 'Colinco ', 'Ijiran', 'melodyijiran0@gmail.com', '$2y$10$.UssbUcWTUQL8wILwRQiqexeeKYMcu/NlqJzEFIPpC5rEJvd0WDzi', '2001-12-08', '2020-12-22 13:33:06', NULL, 0, NULL, 1, 'png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follows`
--
ALTER TABLE `follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgot_passwords`
--
ALTER TABLE `forgot_passwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `follows`
--
ALTER TABLE `follows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
