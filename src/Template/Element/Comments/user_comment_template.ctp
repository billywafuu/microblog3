<div class="card bg-light border-0 my-1" id="comment-<?= $comment['id'] ?>">
    <div class="card-body">
    <?php if($user['id'] == $this->Session->read('Auth.User.id')):?>
        <button type="button" 
            class="close" 
            onclick="deleteComment('<?= $comment['id'] ?>')">
            <span aria-hidden="true">&times;</span>
        </button>
    <?php endif ?>
        <div class="d-flex flex-column">
            <div class="d-flex flex-row">
                <div class="p-1">
                    <a href="<?= $this->request->webroot ?>/users/view/<?= $user['id'] ?>">
                        <img style="width:40px; height:40px" 
                        src="<?= $this->request->webroot ?>user/<?= $user['id'] ?>.<?= $user['image_file_type'] ?>"
                        class="rounded-circle"
                        alt=""
                        onerror="this.src='https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png'"
                        >
                    </a>
                </div>
                <div class="p-1">
                    <h6>
                        <a id="microblog-link" 
                            href="<?= $this->request->webroot?>/users/view/<?= $user['id'] ?>">
                            <?= $user['first_name'] ?> <?= $user['last_name'] ?>
                        </a>
                    </h6>
                    <div>
                        <small class="text-muted"><?= h($comment['comment']) ?></small>
                    </div>             
                </div>
            </div>
        </div>
    </div>
</div>