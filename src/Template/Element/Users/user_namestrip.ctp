<div class="container-fluid">
    <div class="d-flex flex-row">
        <div class="p-2">
        <a href="<?= $this->request->webroot ?>users/view/<?= $owner['id'] ?>">
            <img style="width:50px; height:50px" 
            src="<?= $this->request->webroot ?>user/<?= $owner['id'] ?>.<?= $owner['image_file_type'] ?>" class="rounded-circle" alt=""
            onerror="this.src='https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png'"
        </a>
        </div>
        <div class="p-2">
            <h4>
                <a id="microblog-link" href="<?= $this->request->webroot ?>/users/view/<?= $owner['id'] ?>">
                    <?= $owner['first_name'] ?> <?= $owner['last_name'] ?>
                </a>
            </h4>

        </div>
    </div>
    <hr>
</div>