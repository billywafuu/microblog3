
<div class="card my-4 mx-auto" style="width:450px">
    <div class="card-header">
        <h5><?= __('Login') ?></h5>
    </div>
    <div class="card-body">
        <form method="POST" action="<?= $this->Url->build('/', true) ?>login">
            <div class="form-group">
                <label for="inputEmailAddress"><?= __('Email Address') ?></label>
                <input type="email" class="form-control" name="email_address" aria-describedby="emailHelp" placeholder="<?= __('Enter email address') ?>">
            </div>
            <div class="form-group">
                <label for="inputPassword"><?= __('Password') ?></label>
                <input type="password" class="form-control" name="password" placeholder="<?= __('Password') ?>">
            </div>
            <?php echo $this->Html->link( __('Forgot your password?'),
				array('controller' => '/', 'action' => 'forgotpassword'),
				array('class' => 'dropdown-item text-primary') ); 
			?>
            <div class="text-right">
                <a href="<?= $this->Url->build('/', true) ?>registration" class="btn btn-success"><?= __('Create Account') ?></a>
                <button type="submit" class="btn btn-primary"><?= __('Login') ?></button>
            </div>
        </form>    

    </div>
</div>
