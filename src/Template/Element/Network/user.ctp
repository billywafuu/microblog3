<div class="container-fluid">
<div class="row">
    <div class="col-md-8">
        <div class="d-flex flex-row">
            <div class="p-2">
                <img style="width:50px; height:50px" 
                src="<?= $this->request->webroot ?>user/<?= $user['id'] ?>.<?= $user['image_file_type'] ?>" 
                class="rounded-circle" alt="">
            </div>
            <div class="p-2">
                <h5><?= $user['first_name'] ?> <?= $user['last_name'] ?></h5>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="d-flex flex-column">
            <div class="p-2">
                <a href="<?= $this->request->webroot ?>users/view/<?= $user['id'] ?>" class="badge badge-primary">
                    <i class="fas fa-user"></i> <?= __('See profile')?>
                </a>
            </div>
        </div>
    </div>
</div>
    <hr>
</div>