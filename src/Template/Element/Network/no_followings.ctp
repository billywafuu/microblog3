<div class="container-fluid">
        <div class="card border-0">
            <div class="card-body">
                <h5><i class="fas fa-running"></i> <?= __("You do not follow any users.") ?></h5>
                <p> <?= __("Find someone to follow so you can receive updates from them whenever they post new stuff") ?></p>
                <small class="text-muted"><?= __("Tip: You can search users and post on the search box above!") ?></small>
            </div>
        </div>
</div>