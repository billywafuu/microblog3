<div class="container-fluid">
        <div class="card border-0">
            <div class="card-body">
                <h5><i class="fas fa-sad-tear"></i> <?= __('No user is following you') ?></h5>
                <p><?= __(' No one is following you? They will come do not worry.') ?></p>
                <small class="text-muted"><?= __('Tip: Tell your friends that you are here on Microblog! Invite them!') ?> </small>
            </div>
        </div>
</div>