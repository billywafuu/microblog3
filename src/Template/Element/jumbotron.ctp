<div class="jumbotron">
  <h1 class="display-4"><?=__('Microblog')?></h1>
  <p class="lead"><?= __('The best blogspot ever.') ?></p>
  <p class="lead text-right">
    <a class="btn btn-primary btn-lg" href="<?= $this->Url->build('/', true) ?>registration" role="button"><?= __('Sign Up Now') ?></a>
  </p>
  
</div>