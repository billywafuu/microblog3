<form method="POST" enctype="multipart/form-data">
        <div class="container-fluid p-2">
            <img src="<?= !empty($defaultFields['Post']['image_file_type']) ?  
            $this->request->webroot.'post/'.h($defaultFields['Post']['id']).'.'.h($defaultFields['Post']['image_file_type']) :  '' ;?>" 
            id="previewImage" class="img-fluid">
        </div>
        <?php if(!empty($defaultFields)):?>
            <input type="hidden" name="id" value="<?= h($defaultFields['Post']['id']) ?>">
        <?php endif; ?>
        <div class="form-group">
                <input type="text" value="<?= !empty($defaultFields['Post']['title']) ?  h($defaultFields['Post']['title']) :  '' ;?>" 
                    class="form-control" 
                    name="title" 
                    maxlength="140"
                    aria-describedby="emailHelp" 
                    placeholder="<?= !empty($defaultFields['Post']['title']) ?  h($defaultFields['Post']['title']) :  'My New Post' ;?>"
                >
                <small class="form-text text-muted"><?= __('Enter title of your post.') ?></small>
        </div>
        <div class="form-group">
            <textarea class="form-control" id="input-post-body" name="body" placeholder="<?= !empty($defaultFields['Post']['body']) 
            ?  str_ireplace(array("<br />","<br>","<br/>"),"\r\n", h($defaultFields['Post']['body'])) :'';?>" rows="3" maxlength="140"><?= !empty($defaultFields['Post']['body']) 
            ?  str_ireplace(array("<br />","<br>","<br/>"),"\r\n", $defaultFields['Post']['body']) :'';?></textarea>
            <div class="row">
                <div class="col-6">
                    <small class="form-text text-muted"><?= __('Write something about your post..') ?></small>
                </div>
                <div class="col-6 text-right">
                    <small id="charCounter" class="form-text text-muted">(<?= strlen($defaultFields['Post']['body']) ?>/140)</small>    
                </div>
            </div>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" onchange="previewImagex(this)" name="postImage">
            <label class="custom-file-label" id="addImageLabel" for="customFile"><i class="fas fa-camera"></i>
            <?php if ($defaultFields['Post']['image_file_type'] == "") :?>
                <?= __('Add Image') ?>
            <?php else: ?>
                <?= __('Change Image') ?>
            <?php endif ;?>
             </label>
        </div>
        <?php if ($defaultFields['Post']['image_file_type'] == "") :?>
            <small class="form-text text-muted">
            <?= __('Click to add an image.') ?>
            </small>
        <?php else :?>
            <small class="form-text text-muted">
            <?= __('Click to change the image.') ?>
            </small>
        <?php endif ;?>
        <div class="text-right">             
            <button type="submit" class="btn btn-primary mt-2"><?= __('Share') ?></button>
        </div>
</form>
