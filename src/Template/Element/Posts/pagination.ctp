<div class="container-fluid">
  <ul class="pagination d-flext justify-content-center">
    <li class="page-item <?= ($currentPage == 1) ? 'disabled' : '' ;?>">
      <a class="page-link" href="?page=<?= $prevPage ?>"><?= __('Previous') ?></a>
    </li>
    <?php for($i = 1; $i <= $numberOfPages; $i++):?>
        <li class="page-item <?= ($currentPage == $i) ? 'active' : '' ;?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i?></a></li>
    <?php endfor;?>
    <li class="page-item <?= ($numberOfPages == $currentPage) ? 'disabled' : '' ;?>">
        <a class="page-link" href="?page=<?= $nextPage ?>"><?= __('Next') ?></a>
    </li>
  </ul>
</div>
