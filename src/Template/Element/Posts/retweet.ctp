<?php if ($retweet['deleted'] == 0) :?>
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="d-flex">
                <div class="p-4">
                    <h6 id="postTitle"><?=h($retweet['title'])?></h6>
                    <small id="postDescription"> <?=h($retweet['body'])?></small>
                </div>
            </div>
            <?php if($retweet['image_file_type'] != ""):?>
            <div class="container-fluid text-center">
                <img 
                    src="<?= $this->request->webroot ?>post/<?= h($retweet['id'])?>.<?=h($retweet['image_file_type'])?>" 
                    style="width: 100%; height:auto"
                    id="postImage"
                >
            </div>
            <?php endif;?>
            <div class="p-4">
                    <a href="<?php
                        echo $this->Url->build(
                            [
                                "controller" => "posts",
                                "action" => "view",
                                $retweet['id']
                            ]
                        );
                        ?>"
                    class="badge badge-primary">
                        <i class="fas fa-eye"></i> <?= __('View original post') ?>
                    </a>
            </div>
        </div>
    </div>
</div>
<?php else :?>
<div class="alert alert-warning">
    <h6><?= __('Post unavailable') ?></h6>
    <p><?= __('The post that was retweeted was not found or the owner has taken it down.') ?></p>
</div>
<?php endif; ?>      

