<div class="modal" id="retweetModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fas fa-sync"></i> <?= __('Retweet') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <textarea class="form-control" id="inputRetweetDescription" placeholder="Say something on your retweet.."></textarea>
        <div class="container" id="retweetPreviewContainer">
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="retweet()"><?= __('Retweet') ?></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Cancel') ?></button>
      </div>
    </div>
  </div>
</div>
