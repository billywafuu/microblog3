<div class="d-flex justify-content-center pa-4">
    <div class="text-center">
        <?php if($this->Session->read('Auth.User.id') == $userProfile['User']['id']):?>
            <h5><?= __('You have not published any post') ?></h5>
            <p class="text-muted"><?= __('Create a post and sharing it to your followers is a great way for them to be up to date with you.') ?></p>        
        <?php else:?>
            <h5><?= __('No post to view') ?></h5>
            <p class="text-muted"><?= __('We cannot find posts of ') ?><?= $userProfile['User']['first_name'] ?><?= __('yet') ?>.</p> 
        <?php endif;?>
        <hr>
    </div>
</div>