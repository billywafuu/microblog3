<div class="d-flex justify-content-end">
    <div class="p-1">
        <button type="button" id="btn-retweet-<?= $post_id ?>" 
        onclick="showRetweetModal('<?= $post_id ?>')" class="btn btn-sm btn-outline-primary">
            <i class="fas fa-sync"></i> <?= __('Retweet') ?>
        </button>
    </div>
    <div class="p-1">
        <?php if($hasLiked):?>
            <button type="button" id="btn-like-<?= $post_id ?>" 
            onclick="likePost('<?= $post_id ?>')" class="btn btn-sm btn-outline-danger">
                <i class="far fa-thumbs-down"></i> <?= __('Unlike') ?>
            </button>   
        <?php else:?>
            <button type="button" id="btn-like-<?= $post_id ?>" 
            onclick="likePost('<?= $post_id ?>')" class="btn btn-sm btn-outline-success">
                <i class="far fa-thumbs-up"></i> <?= __('Like') ?>
            </button>
        <?php endif;?>
    </div>
</div>