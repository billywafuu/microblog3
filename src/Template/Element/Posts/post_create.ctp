<div class="container-fluid">
    <div class="card microblog-post">
        <div class="card-body">
            <div class="container-fluid text-center">
                <h4><?= __('Hi') ?> <?= $currentUser['User']['first_name'] ?><?= __(', got something to share?') ?> </h4>
                <button type="button" onclick="showPostCreateForm()" class="btn btn-lg btn-success m-4"> 
                    <i class="fas fa-plus"></i> <?= __('Create Post') ?>
                </button>
            </div>
            <div id="create-post">
                <?= $this->element('Posts/form') ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function (){
        $("#create-post").hide();
    });
    function showPostCreateForm(){
        $("#create-post").toggle();
    }
</script>