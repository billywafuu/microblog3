<form method="POST" action="<?= $this->request->webroot ?>posts/create" enctype="multipart/form-data">
        <div class="container-fluid p-2">
            <img src="<?= !empty($defaultFields['image_file_type']) ?  
            $this->request->webroot .'post/'.$defaultFields['id'].'.'.$defaultFields['image_file_type'] :  '' ;?>" 
            id="previewImage" class="img-fluid">
        </div>
        
        <?php if(!empty($defaultFields)):?>
            <input type="hidden" name="id" value="<?= $defaultFields['id'] ?>">
        <?php endif; ?>
        <div class="form-group">
                <input type="text" value="<?= !empty($defaultFields['title']) ?  $defaultFields['title'] :  '' ;?>" 
                    class="form-control" 
                    name="title" 
                    maxlength="140"
                    aria-describedby="emailHelp" 
                    placeholder="<?= !empty($defaultFields['title']) ?  $defaultFields['title'] :  __('My New Post') ;?>"
                >
                <small class="form-text text-muted"><?= __('Enter title of your post.') ?></small>
        </div>
        <div class="form-group">
            <textarea class="form-control" id="input-post-body" name="body" placeholder="<?= !empty($defaultFields['Post']['body']) 
            ?  str_ireplace(array("<br />","<br>","<br/>"),"\r\n", $defaultFields['body']) :'';?>" rows="3" maxlength="140"><?= !empty($defaultFields['body']) 
            ?  str_ireplace(array("<br />","<br>","<br/>"),"\r\n", $defaultFields['body']) :'';?></textarea>
            <div class="row">
                <div class="col-6">
                    <small class="form-text text-muted"><?= __('Write something about it.') ?></small>
                </div>
                <div class="col-6 text-right">
                    <small id="charCounter" class="form-text text-muted">(0/140)</small>    
                </div>
            </div>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" onchange="previewImagex(this)" name="postImage">
            <label class="custom-file-label" id="addImageLabel" for="customFile">
                <i class="fas fa-camera"></i> <?= __('Add Image') ?>
            </label>
        </div>
        <small class="form-text text-muted"><?= __('Click to add an image to your post.') ?></small>
        <div class="text-right">             
            <button type="submit" class="btn btn-primary mt-2"><?= __('Share') ?></button>
        </div>
</form>
