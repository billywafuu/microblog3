<div id="hp-no-posts" class="d-flex justify-content-center pa-4">
    <div class="text-center">
        <h5><?= __('No posts to show,') ?></h5>
        <p><?= __('There are no posts to show at the moment. Let\'s try again later!') ?></p>
        <hr>
    </div>
</div>