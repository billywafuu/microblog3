<div class="d-flex mt-2 p-2">
    <div class="input-group">
        <input type="text" id="post-<?= $post_id ?>-comment-inp" 
        class="form-control form-control-sm post-comment-box" 
        placeholder="Enter some comment.."
        postid="<?= $post_id ?>"
        onkeypress="if(event.keyCode == 13) postComment('<?= $post_id ?>', true);"
        >
        <div class="input-group-append">
            <button class="btn btn-sm" onclick="postComment('<?= $post_id ?>', true)" type="button">
                <i class="fas fa-paper-plane"></i>
            </button>
        </div>
    </div>
</div>
<div class="container-fluid mt-1 p-2 comment-box-c" id="post-<?= $post_id ?>-comment-box" >
</div>
<div class="container-fluid p-2 text-center">
    <a href="javascript:loadComments('<?= $post_id ?>')" id="a-load-more-comments-<?= $post_id ?>">
        <i class="fas fa-comment"></i> <?= __('Load More Comments') ?>
    </a>
</div>
