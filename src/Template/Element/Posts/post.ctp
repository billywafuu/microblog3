<div id="post-<?=h($post['id'])?>" class="card mb-2 px-0 ">
    <div class="card-body px-0">
    <?= $this->element('Posts/toolbar', array('post' => $post))?>
    <?= $this->element('Users/user_namestrip', array('owner' => $owner)) ?>
            <div class="d-flex">
                <div class="p-3">
                    <small class="text-muted"><?= __('Posted') ?> 
                    <?= date_format(new DateTime($post['created']), 'F d, Y') ?>
                    <?= __('at') ?> 
                    <?= date_format(new DateTime($post['created']), 'g:i a') ?>
                    </small>
                    <h5 id="postTitle"><?=h($post['title'])?></h5>
                    <p id="postDescription"> <?=strip_tags($post['body'], '<br></br><br/>')?></p>
                </div>
            </div>
            <?php if($post['image_file_type'] != ""):?>
            <div class="container-fluid text-center">
                <img 
                    src="<?= $this->request->webroot ?>post/<?= h($post['id'])?>.<?= h($post['image_file_type']) ?>" 
                    style="width: 100%; height:auto"
                    id="postImage"
                >
            </div>
        <?php endif;?>
        <?php if ($retweet !== null): ?>
                <?= $this->element(
                    'Posts/retweet',
                        array(
                            'retweet' => $retweet
                        )
                    ) ?>

        <?php  endif ;?>
        <?php if(count($likes) > 0):?>
            <div class="container-fluid p-3">
                <small class="text-muted" title="<?= count($likes) ?> <?= __('users like this post.') ?>.">
                    <i class="far fa-thumbs-up"></i> <?= count($likes) ?>
                </small>
            </div>
        <?php endif;?>
        <hr>
        <?php
            $hasLiked = false;
            for($i = 0; $i < count($likes); $i++)
            {
                if (isset($likes[$i]['Like']['user_id'])) {
                    if ($likes[$i]['Like']['user_id'] == $this->Session->read('Auth.User.id')) {
                        $hasLiked = true;
                        break;
                    }
                }
                if (isset($likes[$i]['user_id'])) {
                    if ($likes[$i]['user_id'] == $this->Session->read('Auth.User.id')) {
                        $hasLiked = true;
                        break;
                    }
                }
            }
        ?>
        <?= $this->element('Posts/action_bar', array(
            'post_id' =>  $post['id'], 
            'hasLiked' => $hasLiked
        ))?>
        <?= $this->element('Posts/comment_box', array('post_id' => $post['id']))?>
    </div>
</div>