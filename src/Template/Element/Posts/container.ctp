<?= $this->element('Posts/retweeter') ?>
<div class="container-fluid p-0">
    <div class="card border-0 microblog-post">
            <div class="card-body" id="post-container">
                <?php 
                    if($posts === null) {
                        echo $this->element('Posts/no_post');
                    }else{
                        echo $this->element('Posts/pagination', $pagination);
                    }
                ?>    
                <?php 
                    if ($posts !== null) {
                        foreach($posts as $post){
                            echo $this->element('Posts/post', array(
                                "post" => $post,
                                "owner"=> $post->owner,
                                "likes"=> $post->likes,
                                "retweet"=> $post->retweet)
                            );
                        }
                    }  
                ?>
            </div>
    </div>
        <?php 
            if(!empty($posts)){
                echo $this->element('Posts/pagination', $pagination);
            }
        ?>  
</div>
