<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js"></script>
<div class="modal" id="photo_upload" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Upload Photo</h5>
        <button type="button" class="close" id="c-x-button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php if ($currentUser['User']['image_file_type'] !== null) :?>
      <small>You can crop your photo using the white box or zoom according to your preferences. 
      Use the <b>zoom slider</b> to adjust the zoom level of your photo.</small><br><br>
        <center>
        <div id="uploader" class="text-center">
            <div class="spinner-border text-primary p-4 mb-4" role="status">
                <span class="sr-only">Uploading photo..</span>
            </div>
          <h5><i class="far fa-images"></i> Uploading photo..</h5>
          <h6>Please wait while we upload your photo. This might take a while..</h6>
        </div>
        <div id="upload">
          <div id="c_croppie">
            <h5>Crop Existing..</h5>
            <div class="c_image_container" id="c_image_container">
            </div>
          </div>
      
          <br><br>
          <button class="btn btn-sm btn-default" onclick="c_rotate()"><i class="fas fa-redo"></i> Change Orientation</button>
          <?php endif ;?>
          <h5>Or upload a new photo..</h5>
          <input type="file" name="c_file" class="form-control" onchange="c_upload()" id="p_file_upload" accept="image/x-png,image/gif,image/jpeg">
          <small>Replace the current photo of your profile by uploading a new one.</small>
         
        </div>
        </center>
      </div>
      <div class="modal-footer">
        <button type="button" id="c-save-button" class="btn btn-primary" onclick="c_result()">Save</button>
        <button type="button" id="c-close-button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style>
  #c_image_container {
    width:100%;
    height:200px;
  }
</style>
<script>  
var el;
var croppie;
var c_init = 0;
var c_rotate_state = 2;
$(document).ready( function (){
  if(window.location.hash =="#upload_profile_photo"){
    el = document.getElementById('c_image_container');
    croppie = new Croppie(el);
    $('#photo_upload').modal();
  }

  $("#upload").fadeIn();
  $("#uploader").hide();

});

$('#photo_upload').on('shown.bs.modal', function () {
  $('#photo_upload').trigger('focus');
  c_rebind();
}); 

function c_rotate(){
  croppie.bind({
    url: '<?=$this->request->webroot?>user/<?= $currentUser['User']['id'] ?>.<?= $currentUser['User']['image_file_type'] ?>#'+new Date().getTime(),
    orientation: c_rotate_state
  });
  if(c_rotate_state==8){
    c_rotate_state =2 ;
  }else{
    c_rotate_state++;
  }
}

function lockActionButtons(){
    $('#c-save-button').hide();
    $('#c-close-button').hide();
    $('#c-x-button').hide();
}

function unlockActionButtons(){
    $('#c-save-button').show();
    $('#c-close-button').show();
    $('#c-x-button').show();
}

function c_upload(){

  $("#upload").hide();
  $("#uploader").fadeIn();

  var file = document.getElementById('p_file_upload').files[0];

  if(!file){
    $.dialog('<?=__("No file selected for upload.") ?>', '<i class="far fa-images"></i> <?=__("Upload Failed") ?>');
    return;
  }

  var c = new FormData();
  c.append('fileToUpload',file);
  c.append('uploadAction','new_upload');
  $.ajax({
    url: bu() + 'users/uploadimage',
    data:c,
    type:'post',
    datatype:'html',
    contentType:false,
    processData:false,
    beforeSend: ()=> {
        lockActionButtons();
    },
    success: function(response){
      unlockActionButtons();
      response = JSON.parse(response);

      if(response.result == "upload_success"){
        location.reload();
        $("#upload").fadeIn();
        $("#uploader").hide();
      }

      if(response.result == "invalid_file_upload"){
        $.dialog(
            "<?=__("No file selected for upload.") ?>",
            '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );
        return;
      }

      if(response.result == "p_file_not_image"){
        $("#upload").fadeIn();
        $("#uploader").hide();

        $.dialog(
        "<?=__("The file is not an image. Please only upload image files not larger than 2MB.") ?>",
        '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );

      }
      if(response.result == "p_upload_failed"){
        $.dialog(
        "<?=__('There has been an error while trying to upload the file. Please try again later.') ?>",
        '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );
      }
    },
    error: ()=> {
        $.dialog(
            "<?=__('There has been an error while trying to upload the file. Please try again later.') ?>",
            '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );

        $("#upload").fadeIn();
        $("#uploader").hide();
        unlockActionButtons();
    }
  });
}
function c_result(){
  croppie.result(
    'base64',
    'viewport',
    'png',
    1,
    true
  ).then(function(b64){
    var c = new FormData();
    c.append('fileToUpload',b64);
    c.append('uploadAction','crop_existing');

  $.ajax({
    url: bu() + 'users/uploadimage',
    data:c,
    type:'post',
    datatype:'html',
    contentType:false,
    processData:false,
    success: function(response){
      response = JSON.parse(response);
      if(response.result == "upload_success"){
        location.reload();
        $("#upload").fadeIn();
        $("#uploader").hide();
      }

      if(response.result == "invalid_file_upload"){
        $.dialog(
            "<?=__("No file selected for upload.") ?>",
            '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );
        return;
      }
      if(response.result == "invalid_file_type"){
        $.dialog(
            "<?=__("The file you uploaded is not supported.") ?>",
            '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );
        return;
      }
      if(response.result == "p_file_not_image"){
        $("#upload").fadeIn();
        $("#uploader").hide();
        $.dialog(
            "<?=__("The file is not an image.") ?>",
            '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );
      }
      if(response.result == "p_upload_failed"){
        $.dialog(
            "<?=__("There was an error while trying to upload. Please try again.") ?>",
            '<i class="far fa-images"></i> <?=__("Upload Failed") ?>'
        );
      }
    }
  });
  });
}
function c_rebind(){
    container = document.getElementById('c_croppie');
    el = document.getElementById('c_image_container');
    container.removeChild(el);
    croppie = null;
    var element = document.createElement("div");
    element.id="c_image_container";
    container.appendChild(element);
    el = document.getElementById('c_image_container');
    croppie = new Croppie(el, {
      viewport: { width: 200, height: 200,type: "circle" },
      boundary: { width: 300, height: 200 },
      showZoomer: true,
      enableOrientation: true
    });
    croppie.bind({
      url: '<?=$this->request->webroot?>user/<?= $currentUser['User']['id'] ?>_original.<?= $currentUser['User']['image_file_type'] ?>?' + new Date().getTime()
    });
}
</script>
