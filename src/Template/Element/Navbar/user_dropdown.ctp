<?php
    $filename = $this->request->webroot
    . 'user/' . $currentUser['User']['id'] 
    . '.' . $currentUser['User']['image_file_type'];
?>
<div class="d-flex">
	<div class="p-1">
		<a href="<?= $this->Url->build('/', true) ?>profile">
			<img src="<?= $filename ?>?<?= date('Y-m-d H:i:s')?>" class="rounded-circle" 
			style="width:30px; height: 30px"
			onerror="this.src='https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png'"
			>
		</a>
	</div>
	<div>
		<div class="dropdown">
			<button class="btn btn-transparent dropdown-toggle text-light mr-4" 
				type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<strong><?= $currentUser['User']['first_name'] ?></strong>
			</button>
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<?php echo $this->Html->link( __("My Profile"),
					array('controller' => 'users', 'action' => 'profile'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( __("Settings"),
					array('controller' => 'users', 'action' => 'settings'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( __("Change Password"),
					array('controller' => 'users', 'action' => 'changepassword'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( __("My Network"),
					array('controller' => 'users', 'action' => 'network'),
					array('class' => 'dropdown-item') ); 
				?>
				<?php echo $this->Html->link( __("Log out"),
					array('controller' => 'microblog', 'action' => 'logout'),
					array('class' => 'dropdown-item') ); 
				?>
			</div>
		</div>
	</div>
</div>
