<form method="POST" action="<?= $this->Url->build('/lang', true); ?>" id="form-lang-selector">
<div class="form-group">
    <select class="form-control form-control-sm" id="btn-lang-selector" name="lang-selector">
      <option value="en_US">EN</option>
      <option value="ja_JA">JP</option>
      <option value="en_PH">PH</option>
    </select>
  </div>
</form>
<script>
  $('#btn-lang-selector').change(()=> {
    g('form-lang-selector').submit();
  });
</script>