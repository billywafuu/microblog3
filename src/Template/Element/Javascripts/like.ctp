<script>
function likePost(post_id){
    $.ajax({
        url: bu() + 'likes/like',
        data: { post_id: post_id }, 
        datatype: 'json',
        type: 'post',
        beforeSend: ()=>{

            // Suspend button interaction before sending the AJAX request.
            $("#btn-like-" + post_id).prop('readonly', true);

        },
        success: (response)=>{

            response = JSON.parse(response);

            if(response.result == "success"){

                /*
                    If the server thrown a liked state, 
                    make the button unlike for the next interaction.
                */

                if(response.state == 'liked'){

                    $("#btn-like-" + post_id).html('<i class="far fa-thumbs-down"></i> Unlike')
                    .removeClass('btn-outline-success')
                    .addClass('btn-outline-danger');   

                }else{

                    $("#btn-like-" + post_id).html('<i class="far fa-thumbs-up"></i> Like')
                    .removeClass('btn-outline-danger')
                    .addClass('btn-outline-success');   

                }

    
            }else{

                /*
                    If the response of the server is not success, show a dialog on the client side.
                */

                $.dialog("There has been an error while liking the post. Please try again later.", "Like");


            }

            
            $("#btn-like-" + post_id).prop('readonly', false);
        },
        error: ()=> {
            $.dialog("There has been an error while liking the post. Please try again later.", "Like");
        }
    })
}
</script>