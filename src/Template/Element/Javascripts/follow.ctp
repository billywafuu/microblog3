<script>
function followUser(id){
    $.ajax({
        url: bu() + 'follows/follow',
        data: {user_id: id},
        datatype: 'json',
        type: 'post',
        success: (response)=>{
            response = JSON.parse(response);
            if(response.result == "follow_success"){
                $("#btn-follow-" + id).html('<i class="fas fa-user-minus"></i> Unfollow')
                .removeClass('btn-primary')
                .addClass('btn-danger');   

                $.dialog("<?= __('You have followed ') ?>" + response.userName + 
                ". <?= __('Any post made by this user will be automatically pushed on your feeds.') ?>", '<i class="fas fa-user-plus"></i> <?= __('Follow') ?>');
                return;
            }
            if(response.result == "unfollow_success"){

                $("#btn-follow-" + id).html('<i class="fas fa-user-plus"></i> <?= __('Follow') ?>')
                .removeClass('btn-danger')
                .addClass('btn-primary');   

                $.dialog("<?= __('You have successfully unfollowed ') ?>" + response.userName + 
                " <?= __('and will no longer appear on your feeds.') ?>", '<i class="fas fa-user-minus"></i> <?= __('Unfollow') ?> ');
                return;
            }

            $.dialog("An error has occurred while performing this operation. Please try again later.", "Follow/Unfollow");
            return;
        },
        error: ()=> {
            $.dialog("An error has occurred while performing this operation. Please try again later.", "Follow/Unfollow");
            return;
        }
        
    })
}
</script>