<script>
$(document).ready(()=>{
    var inputPostBody = $("#input-post-body");
    if (inputPostBody) {
        var inputCharCounter = $("#charCounter");
        if(!inputCharCounter){
            return;
        }
        $(inputPostBody).keyup(()=>{
            $(inputCharCounter).html('('+ $(inputPostBody).val().length + '/ 140)');
        })
        return;
    }
    console.log("not found");
})

function deletePost(id){
    $.confirm({
        title: '<?= __('Delete post?') ?>',
        content: '<?= __('Are you sure that you want to delete this post? If you want to change something, you can just edit it. Continue?') ?>',
        buttons: {
            confirm: 
            {   text: 'Delete',
                btnClass: 'btn-danger',
                keys: ['del'],
                action: function(){
                    $.ajax({
                        url: bu() + 'posts/delete',
                        data: { post_id: id },
                        datatype: 'json',
                        type: 'POST',
                        success: (response) => {
                        
                            response = JSON.parse(response);
                            if(response.result == "delete_success"){
                                $('#post-'+id).remove();
                            }
                            if(response.result == "unauthorized_operation"){
                                $.dialog("That's weird because the post that you want to delete does not belong to you.", 
                                'Oopss..');
                            }
                        }
                    })
                }
            },
            cancel: function () {
                return;
            },
            somethingElse: {
                text: 'Edit',
                btnClass: 'btn-blue',
                keys: ['enter', 'shift'],
                action: function(){
                    
                }
            }
        }
    });
}
function editPost(id){
    window.location = bu()+"posts/edit/"+id;
}

function previewImagex(input){

    if(input.files && input.files[0]){

        var reader = new FileReader();

        reader.onload = function(e) {
            document.getElementById('previewImage').src = e.target.result;
            document.getElementById('addImageLabel').innerHTML = '<i class="fas fa-camera"></i> Change Image';
        }       

        reader.readAsDataURL(input.files[0]);
    }

}
function showRetweetModal(id){

    // Clear the container from possible leftover from previous retweet. 
    g("retweetPreviewContainer").innerHTML = "";

    // Clear the textarea. 
    g("inputRetweetDescription").value = "";

    // Early bail out if there is no id passed.
    if(id == ""){

        $.dialog("<?= __('There is nothing to retweet.') ?>", 
        '<i class="fas fa-sync"></i> <?= __('Retweet') ?>');

        return;
    }

    // At this point we will perform to retrieve the post manually.
    // If the ID was not found on the DOM then possibly the user
    // is messing up and making custom forging request. Exit.

    var post = g("post-"+ id);

    if(post == null){

        $.dialog("<?= __('There is nothing to retweet.') ?>", 
        '<i class="fas fa-sync"></i> <?= __('Retweet') ?>');

        return;

    }

    // Put the post inside the modal so the user knows
    // the post that is going to be retweeted but we have to
    // reconstruct the title, description and image only.  

    // Build all the necessary divs.
    var externalContainer    = document.createElement('div');
    var titleContainer       = document.createElement('div');
    var descriptionContainer = document.createElement('div');

    // Put a class for the divs. 
    externalContainer.className    = "container-fluid card mt-2";
    titleContainer.className       = "container-fluid p-2 my-1";
    descriptionContainer.className = "container-fluid p-2";


    // Create the title. 
    var title = document.createElement('h5');

    // Copy the title from the source post.
    title.innerHTML = post.querySelector("#postTitle").innerHTML;

    // Insert the title to its container. 
    titleContainer.appendChild(title);

    // Create the description. 
    var description = document.createElement('p');

    // Copy the description from the source post.
    description.innerHTML = post.querySelector("#postDescription").innerHTML;

    // Insert the description to its container. 
    descriptionContainer.appendChild(description);

    // Create the hidden id input. 
    var hiddenId = document.createElement('input');
    hiddenId.type = "hidden";
    hiddenId.value = id;
    hiddenId.id = "retweet-post-id";

    // Append the divs inside the externalContainer.
    externalContainer.appendChild(titleContainer);
    externalContainer.appendChild(descriptionContainer);
    externalContainer.appendChild(hiddenId);

    // Check if there is an image for this post.
    hasImage = post.querySelector("#postImage");

    if(hasImage){
        var imageContainer = document.createElement('div');
	    imageContainer.className = "container-fluid p-1";    
		
        // Create the description. 
        var img = document.createElement('img');

        // Copy the src from the source image. 
        img.src = hasImage.src;

        // Apply the style;
        img.style = "width: 100%; height:auto";

        // Insert to the container. 
        imageContainer.append(img);
        
        externalContainer.appendChild(imageContainer);
    }

    // Insert the externalContainer inside the div of the modal. 
    $("#retweetPreviewContainer").append(externalContainer);

    // Fire the modal. 
    $("#retweetModal").modal('show');

}

function retweet(){
    
    // Get the post id to retweet from the hidden field.
    var id = g("retweet-post-id").value;

    if(id==""){

        $.dialog("<?= __('There is nothing to retweet.') ?>", 
        '<i class="fas fa-sync"></i> <?= __('Retweet') ?>');

        return;

    }

    // Fire up AJAX and send the id to the retweet controller. 
    $.ajax({

        url: bu() + 'posts/retweet',
        data: {
            post_id: id,
            input: g('inputRetweetDescription').value
        },
        datatype: 'json',
        type: 'post',

        success:(response) => {
            
            response = JSON.parse(response);
            if(response.result == "retweet_success"){

                $("#retweetModal").modal('hide');

                // Perform cleaning operations.
                // Clear the container from possible 
                // leftover from previous retweet. 
                g("retweetPreviewContainer").innerHTML = "";

                // Clear the textarea. 
                g("inputRetweetDescription").value = "";

                $.dialog("<?= __('The post has been successfully retweeted and now on your profile.') ?>", 
                '<i class="fas fa-sync"></i> <?= __('Retweet Success') ?>');

                return;

            }
            if(response.result == "post_not_found"){

                $("#retweetModal").modal('hide');

                // Perform cleaning operations.
                // Clear the container from possible 
                // leftover from previous retweet. 
                g("retweetPreviewContainer").innerHTML = "";

                // Clear the textarea. 
                g("inputRetweetDescription").value = "";

                $.dialog("<?= __('The post that you are trying to retweet may have been removed by the owner or does not actually exist.') ?>", 
                '<i class="fas fa-sync"></i><?= __('Retweet Failed') ?> ');
                return;
            }
        },
        error: ()=> {

            $.dialog("An error has occurred while trying to retweet the post. Please try again later.", 
            '<i class="fas fa-sync"></i> Retweet oopsy..');
    
            return;
        }
    });
}
</script>