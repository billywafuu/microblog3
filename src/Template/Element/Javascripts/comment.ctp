<script>
function postComment(post_id, prependOnSend){
    var commentInput = g("post-" + post_id + "-comment-inp");
    
    if(commentInput.value == ""){
        $.dialog("<?= __('Please enter a valid comment.') ?>");
        return;
    }

    $.ajax({
        url: bu() + 'comments/post',
        data: { 
            post_id: post_id,
            comment: commentInput.value
        }, 
        datatype: 'json',
        type: 'post',
        beforeSend: ()=>{
            $("#post-" + post_id + "-comment-inp").prop('readonly', true);
        },
        success: (response)=>{
            response = JSON.parse(response);
            if(response.result == "success"){
                commentInput.value = "";
                if(prependOnSend == true){
                    $("#post-" + post_id + "-comment-box").prepend(response.html);
                }    
            }else{
                $.dialog("<?= __('There has been an error while posting your comment. Please try again later.') ?>", "Oh shhh..ems");
            }
            $("#post-" + post_id + "-comment-inp").prop('readonly', false);
        },
        error: ()=> {
            $.dialog("<?= __('There has been an error while posting your comment. Please try again later.') ?>", "Oh shhh..ems");
        }
    })
}

var currentPostId = -1;
var currentCommentPage = 1;

function loadComments(post_id){
    if(currentPostId != post_id){

        // Clear any comments from other posts 
        // to free up the DOM size. 
        $('.comment-box-c').html('');
        currentCommentPage = 1;
        $("#a-load-more-comments-" + currentPostId).show();
        currentPostId = post_id;

    }
    $.ajax({
        url: bu() + 'comments/load',
        data: { 
            post_id: post_id,
            page: currentCommentPage,
        }, 
        datatype: 'json',
        type: 'post',
        beforeSend: ()=>{

            createSpinner(currentPostId);

        },
        success: (response) => {

            response = JSON.parse(response);

            if(response.result == "success"){

                $("#post-" + post_id + "-comment-box").append(response.html);
                $(".comment-spinner").remove();
                currentCommentPage ++ ;

                if(response.loadMoreComments == false){
                    $("#a-load-more-comments-" + post_id).hide();
                }
            }
        },
        error: () => {

            $.dialog("There has been an error while loading the comments of this post. Please try again later.", "Wait, what?");
            return;

        }
    })
}

function createSpinner(post_id){
    var div = document.createElement('div');
    div.className = "container-fluid text-center";
    div.id = 'post-spinner-'+post_id;
    var spinner = document.createElement('div');
    spinner.className = "spinner-border text-primary comment-spinner";
    spinner.role = "status";
    var span = document.createElement('span');
    span.className = 'sr-only';
    span.innerHTML = 'Loading comments...';
    spinner.appendChild(span);
    div.appendChild(spinner);
    g("post-" + post_id + "-comment-box").appendChild(div);
}
function destroySpinner(post_id){
    var spinner = $("#post-spinner-"+post_id);
    if(spinner){
        spinner.remove();
    }
}
function deleteComment(id){
    $.confirm({
        title: '<i class="fas fa-comments"></i> <?= __('Delete Comment') ?>?',
        content: "<?= __('Are you sure you wish to delete this comment?') ?>",
        buttons: {
            confirm: {
                text: "Yes, delete",
                btnClass: "btn-danger",
                action: ()=>{
                    $.ajax({
                        url: bu() + 'comments/delete',
                        data: { 
                            id: id
                        }, 
                        datatype: 'json',
                        type: 'post',
                        success: (response)=>{
                            response = JSON.parse(response);
                            if(response.result == "success"){
                                var comment = $("#comment-"+id);
                                if(comment){
                                    comment.remove();
                                }
                                $.dialog("<?= __('You successfully deleted the comment.') ?>");
                            }else{
                                $.dialog("There has been an error while deleting your comment. Please try again later.", "Oh shhh..ems");
                            }
                            $("#post-" + post_id + "-comment-inp").prop('readonly', false);
                        },
                        error: ()=> {
                            $.dialog("There has been an error while deleting your comment. Please try again later.", "Oh shhh..ems");
                        }
                    })
                }
            },
            cancel: {

            }
        }
    })
}
</script>