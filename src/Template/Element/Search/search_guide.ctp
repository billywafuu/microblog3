<div class="container-fluid text-center p-4" id="search-guide">
    <h4><i class="fas fa-search"></i> <?= __('Enter keywords to search') ?></h4> 
    <small class="text-muted"><?= __('Enter any text to continue to search for users or posts. It is that easy!') ?></small>
</div>