<div class="container-fluid">
<div class="row">
    <div class="col-md-8">
        <div class="d-flex flex-row">
            <div class="p-2">
                <img style="width:50px; height:50px" src="<?= $this->request->webroot ?>user/<?= $user['id'] ?>.<?= $user['image_file_type'] ?>" 
                class="rounded-circle" alt=""
                onerror="this.src='https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png'">
            </div>
            <div class="p-2">
                <h4><?= $user['first_name'] ?> <?= $user['last_name'] ?></h4>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="d-flex flex-row flex-row-reverse">
            <div class="p-2">
                <a href="<?= $this->request->webroot ?>users/view/<?= $user['id'] ?>" class="badge badge-primary">
                    <i class="fas fa-user"></i> <?= __('See Profile') ?>
                </a>
            </div>
        </div>
    </div>
</div>
    <hr>
</div>