<div class="container-fluid">
  <ul class="pagination d-flext justify-content-center">
    <li class="page-item <?= ($currentPage == 1) ? 'disabled' : '' ;?>">
      <a class="page-link" href="javascript:<?= $function ?>(<?= $prevPage ?>)"><?= __('Previous') ?></a>
    </li>
    <?php for($i = 1; $i <= $maxNumberOfPages; $i++):?>
        <li class="page-item <?= ($currentPage == $i) ? 'active' : '' ;?>">
          <a class="page-link" href="javascript:<?= $function ?>(<?= $i ?>)"><?= $i?></a>
        </li>
    <?php endfor;?>
    <li class="page-item <?= ($maxNumberOfPages == $currentPage) ? 'disabled' : '' ;?>">
        <a class="page-link" href="javascript:<?= $function ?>(<?= $nextPage ?>)"><?= __('Next') ?></a>
    </li>
  </ul>
</div>
