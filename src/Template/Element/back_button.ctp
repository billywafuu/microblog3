<div class="container text-right">
    <button class="btn btn-link waves-effect" style="font-size:24px; padding:10px" onclick="window.history.back()">
        <i class="fas fa-arrow-left"></i> <?=__("Back") ?>
    </button>
</div>