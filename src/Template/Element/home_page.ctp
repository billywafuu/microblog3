<?= $this->element('croppie') ?>
<?= $this->element('Posts/retweeter') ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <?= $this->element('Posts/post_create') ?>
            <div class="card border-0 microblog-post">
                <div class="card-body">
                    <div class="container-fluid text-center">
                        <div class="spinner-border text-primary" id="top-spinner">
                            <span class="sr-only"><?= __('Loading...') ?></span>
                        </div>
                    </div>
                    <div id="post-container">
                    
                    </div>
                    <div class="container-fluid text-center">
                        <div class="spinner-border text-success" id="bottom-spinner">
                            <span class="sr-only"><?= __('Loading...') ?></span>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

    </div>
</div>
<script>
var lastUpdatedTimestamp = 0;
var currentPage = 0;
var postRetriever;
var post_container;
var endOfFeeds = false;
var init = 0;
var topSpinner;
var bottomSpinner;
$(document).ready(() =>{
    post_container = $("#post-container")
    topSpinner = $("#top-spinner");
    bottomSpinner = $("#bottom-spinner");
    topSpinner.addClass('d-none');
    bottomSpinner.addClass('d-none');
    topSpinner.hide();
    nextPage();
    $(window).scroll(
        $.debounce(1000, (e)=> {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                nextPage();
            }  
        })
    );
    postRetriever = setInterval(()=> {
        if ($(window).scrollTop() < 10) {
            retrieveNewPosts();
        } else {
            console.log("Abandoned retrieving new post since user is not on top.");
        }
    },3000);
})
function nextPage(){
    if(endOfFeeds){
        console.log("All of the feeds has been retrieved.");
        return;
    }
    console.log("Retrieving page " + ( currentPage + 1 ) + " of posts.");
    $.ajax({
        url: bu() + 'microblog/index',
        data: { page: currentPage + 1},
        datatype: 'json',
        type: 'post',
        beforeSend: () => {
            bottomSpinner.removeClass('d-none');
        },
        success: (response) => {
            $("#hp-no-posts").remove();
            response = JSON.parse(response);
            post_container.append(response.html);
            currentPage = parseInt(response.currentPage);
            endOfFeeds = response.endOfFeeds;
            if (init == 0) {
                init = 1;
            }
            if (lastUpdatedTimestamp == 0) {
                if (response.timestamp){
                    lastUpdatedTimestamp = response.timestamp;
                }
                console.log("Updated timestamp to " + lastUpdatedTimestamp);
            }
            bottomSpinner.addClass('d-none');
        },
        error: () => {
            $.dialog("There has been an error while trying to retrieve new posts.");
            return;
        }
    })
}
function retrieveNewPosts(){

    if (init == 0) {
        return;
    }
    console.log("Retrieving new posts higher than "+ lastUpdatedTimestamp +" from followers.");
    $.ajax({
        url: bu() + 'microblog/index',
        data: { ts: lastUpdatedTimestamp },
        datatype: 'json',
        type: 'post',
        beforeSend: () => {
            topSpinner.removeClass('d-none');
        },
        success: (response) => {
            response = JSON.parse(response);
            if(response.result == "new_posts"){
                post_container.prepend(response.html);
                lastUpdatedTimestamp = response.timestamp;
                $("#hp-no-posts").remove();
            }
            topSpinner.addClass('d-none');
        },
        error: () => {
            //$.dialog("There has been an error while trying to retrieve new posts.");
            return;
        }
    })
}
</script>
