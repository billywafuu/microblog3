<div class="d-flex justify-content-center pa-4">
    <div class="text-center">
            <h5><i class="fas fa-globe"></i> <?= __('No network of users yet') ?></h5>
            <p class="text-muted"><?= __('To view posts from other users, you need to follow other users and the same thing that they will follow you back.') ?></p>
            <?= $this->Html->link( __('Search Users to Follow'),
                array('controller' => '/', 'action' => 'search'),
                array('class' => 'btn btn-primary'));  
            ?>
    </div>
</div>