<div class="container-fluid">
        <div class="card microblog-view">
            <div class="card-body">
                <h4><?= __('Post Not found') ?></h4>
                <p> <?= __('The post that you are trying to view was not found. It may have been removed by the owner.') ?></p>
                <small class="text-muted"><?= __('If you think this was a mistake, please don not contact us. We do not care.') ?> </small>
                <?= $this->element('back_button') ?>
            </div>
        </div>
</div>