<div class="container">
    <div class="card my-4 mx-auto" style="width: 450px;">
    <div class="card-body">
        <h4 class="card-title"><?= $this->element('back_arrow')?> <?= __('Create Post') ?></h4>
        <h6 class="card-subtitle mb-2 text-muted"><?= __('Share a new post to your followers.') ?></h6>
        <?= $this->element('Posts/form') ?>
    </div>
    </div>
</div>
