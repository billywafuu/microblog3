<?= $this->element('Posts/retweeter') ?>
<div class="container-fluid mt-2">
    <div id="post-<?=h($post['Post']['id'])?>" class="card mb-2 px-0 microblog-view">
        <div class="card-body px-0">
        <?= $this->element('Posts/toolbar', array('post' => $post['Post']))?>
        <?= $this->element('Users/user_namestrip', array('owner' => $post['Owner'])) ?>
            <div class="d-flex">
                <div class="p-4">
                    <h5 id="postTitle"><?=h($post['Post']['title'])?></h5>
                    <p id="postDescription"> <?=strip_tags($post['Post']['body'], '<br></br><br/>')?></p>
                </div>
            </div>
            <?php if($post['Post']['image_file_type'] != ""):?>
            <div class="container-fluid text-center">
                <img 
                    src="<?= $this->request->webroot ?>post/<?= h($post['Post']['id'])?>.<?= h($post['Post']['image_file_type'])?>" 
                    style="width: 100%; height:auto" id="postImage"
                >
            </div>
            <?php endif;?>
            <?php if($post['Retweet'] !== null) :?>
                <?= $this->element(
                    'Posts/retweet',
                        array(
                            'retweet' => $post['Retweet']
                        )
                    ) ?>
            <?php else: ?>
                <?php if($post['Post']['post_id'] != "") :?>
                    <div class="alert alert-warning">
                        <h6><?= __('Post Unavailable') ?></h6>
                        <p><?= __('The post that was retweeted was not found or the owner has taken it down.') ?></p>
                    </div>
                <?php endif ;?>
            <?php endif ;?>
            <hr>
            <?php
                $hasLiked = false;
                for($i = 0; $i < count($post['Likes']); $i++)
                {
                    if ($post['Likes'][$i]['user_id'] == $this->Session->read('Auth.User.id')) {
                        $hasLiked = true;
                        break;
                    }
                }
            ?>
            <?= $this->element(
                'Posts/action_bar', array(
                    "hasLiked" => $hasLiked, 
                    "post_id" => h($post['Post']['id']
                    )
                )
            )?>  
            <div class="container p-4">
                <div class="alert alert-light" role="alert">
                    <p><strong><i class="fas fa-comments"></i> <?= __('Start a conversation on this post') ?> </strong> <br><?= __('New comments are automatically retrieved.') ?></p>
                </div>         
            </div>
            <?= $this->element('Posts/ajax_comment_box', array("post_id" => h($post['Post']['id'])))?>
        </div>
    </div>
</div>

<script>
var lastUpdatedTimestamp = 0;
var commentRetriever;
var commentBox;
$(document).ready(() => {
    commentBox = $("#post-<?= $post['Post']['id'] ?>-comment-box");
    $("#a-load-more-comments-<?= $post['Post']['id'] ?>").hide();
    createSpinner('<?=$post['Post']['id'] ?>');
    commentRetriever = setInterval(()=>{ 
        $.ajax({
            url: bu() + 'comments/retrieve',
            data: { 
                post_id: '<?= $post['Post']['id'] ?>',
                timestamp: lastUpdatedTimestamp
            }, 
            datatype: 'json',
            type: 'post',
            success: (response) => {
                response = JSON.parse(response);
                destroySpinner('<?=$post['Post']['id'] ?>');
                $("#a-load-more-comments-<?= $post['Post']['id'] ?>").show();
                console.log(response);
                if(response.result == "success"){
                    if(response.update == "new_comments"){
                        commentBox.prepend(response.html);
                        lastUpdatedTimestamp = response.timestamp;
                    }
                }
            }, 
            error: ()=> {
                $.dialog("<?= __('Unfortunately, it seems that we cannot pull new comments for this thread.') ?>", "Dang!");
                clearInterval(commentRetriever);
            }
        })
    }, 2000);
})
</script>