<div class="container">
    <div class="card my-4 mx-auto" style="width: 450px;">
        <div class="card-body">
            <h4 class="card-title"><?= $this->element('back_arrow')?> <?= __('Edit Post') ?></h4>
            <h6 class="card-subtitle mb-2 text-muted"><?= __('Change the title, body or the image of your post.') ?></h6>
            <?= $this->element('Posts/edit_form', 
                array('defaultFields' => $postData)
            )?>
        </div>
    </div>
</div>

