<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><i class="fas fa-envelope"></i> <?= __('We sent you an email') ?></h5>
                <p class="card-text"><?= __('An email has been sent to ') ?>
                <?= h($this->getRequest()->getSession()->consume('Activation.email')) ?> 
                <?= __('for activation. Check your inbox and click the provided link to activation your account for use.') ?> 
                </p>
            </div>
        </div>
    </div>
</div>
