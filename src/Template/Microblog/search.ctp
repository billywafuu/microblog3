<div class="container-fluid">
    <div class="card microblog-view">
        <div class="card-body">
            <h5><?= $this->element('back_arrow')?> <?= __('Search') ?></h5>
            <?php if(isset($result)):?>
                <?php if($result != null):?>
                    <?php if($result['overallCount'] != 0): ?>
                        <small class="text-muted"><?= __('Showing') ?> <text id="search-result-count"><?= count($result['returnRows']) ?></text> 
                        <?= __('out of') ?> <text id="search-result-count"><?= $result['overallCount'] ?></text></small>
                    <?php else: ?>
                        <small class="text-muted"><?= __('There are no results found on your search.') ?></small>
                    <?php endif ?>
                <?php endif;?>
            <?php endif;?>
            <div class="container-fluid">
                <input type="text" class="form-control mt-2" id="ajaxSearchInput"
                value="<?= ($this->Session->check('Search.q')) ? h($this->Session->read('Search.q')) : ''?>"
                placeholder="<?= __('Enter keywords here..') ?>">
                <div class="d-flex flex-row">
                    <div class="p-2">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" onchange="search(1)" name="searchTarget" value="users" 
                            <?= ($this->Session->read('Search.target') == "users") ? 'checked' : ''?>
                            <label class="form-check-label"><?= __('Users') ?></label>
                        </div>                   
                    </div>
                    <div class="p-2">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" onchange="search(1)" name="searchTarget" value="posts"
                            <?= ($this->Session->read('Search.target') == "posts") ? 'checked' : ''?>
                            >
                            <label class="form-check-label"><?= __('Posts') ?></label>
                        </div>
                    </div>
                </div>
                <small class="text-muted">
                <?= __('You can set your search target above if you want to search for users or posts.') ?>
                </small>
                <div class="container-fluid p-4 text-center" id="search-loader">
                    <div class="spinner-border text-success p-4" role="status">
                        <span class="sr-only"><?= __('Loading') ?>...</span>
                    </div>
                </div>

                <hr>
                <div class="container-fluid" id="result-container">
                    <?php 
                        if(isset($result)){
                            if($result != null){
                                if ($result['overallCount'] != 0) {
                                    if($result['model'] == 'users'){
                                        foreach($result['returnRows'] as $user){
                                            echo $this->element('Search/user_result', array('user' => $user));
                                        }
                                    }else if($result['model'] == 'posts'){
                                        foreach($result['returnRows'] as $post){
                                            echo $this->element('Search/post_result', array('post' => $post));
                                        }                            
                                    }
                                } else {
                                    echo $this->element('Search/search_empty');
                                }
                            }else{
                                echo $this->element('Search/search_guide');
                            }
                        }else{
                            echo $this->element('Search/search_guide');
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script> 
    $(document).ready(()=>{

        // Clear the navbar's search textbox.
        g('searchQuery').value = "";

        // Hook up a keyup event that fires every 500 ms instead on every key up.
        $("#ajaxSearchInput").keyup($.debounce(500, function(e) {
            search(1);
        }));    

        // Hide the spinner for AJAX search.
        $("#search-loader").hide();
 
        // if(g('ajaxSearchInput').value != ""){
        //     search(1);
        // }

    })

    function search(page){

        // Destroy the search guide if it exists.
        var search_guide = $('#search-guide');

        if(search_guide){
            search_guide.remove();
        }

        // Get target between users or posts.
        var target = $("input[name='searchTarget']:checked").val();
        
        if(target == ""){
            return;
        }

        if(page < 0){
            return;
        }

        $.ajax({
            url: bu() + "search",
            data: {
                searchQuery: g("ajaxSearchInput").value,
                searchPage: page,
                searchTarget: target
            },
            datatype: 'json',
            type: 'post',
            beforeSend: () => {
                $("#search-loader").show();
            },
            success: (response) => {
                response = JSON.parse(response);
                if(response.result == "success"){
                    $("#result-container").html(response.html);
                }else{
                    $.dialog("There has been an error while trying to search for " + target + ". Please try again later.", "That's embarassing..");
                }
                $("#search-loader").hide();
            },
            error: () => {
                $("#search-loader").hide();
                $.dialog("There has been an error while trying to search for " + target + ". Please try again later.", "That's embarassing..");
                return;
            }
        })

    }
</script>