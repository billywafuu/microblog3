<div class="container">
<div style="width:450px" class="card mx-auto my-4">
    <div class="card-header">
        <h5> <?= $this->element('back_arrow') ?><?= __('Registration') ?></h5>
    </div>
    <div class="card-body">
        <form method="POST" id="registrationForm">
            <input type="hidden" name="_csrfToken" value="<?= $this->request->getParam('_csrfToken') ?> ">
            <div class="mb-3">
                <input type="text" name="first_name" class="form-control" placeholder="<?= __('First Name') ?>"
                    value="<?= (isset($form['first_name'])) ? h($form['first_name']) : '' ; ?>"
                >
                <?php if (isset($errors['first_name']['_empty'])) :?>
                    <small class="text-danger"><?= h($errors['first_name']['_empty']) ?></small>
                <?php elseif (isset($errors['first_name']['invalidFormat'])) :?>
                    <small class="text-danger"><?= h($errors['first_name']['invalidFormat']) ?></small>
                <?php else :?>
                    <small class="text-muted"><?= __('Enter your first name.') ?></small>
                <?php endif ;?>
            </div>
            <div class="mb-3">
                <input type="text" name="middle_name" class="form-control" placeholder="<?= __('Middle Name') ?>"
                    value="<?= (isset($form['middle_name'])) ?  h($form['middle_name']) : '' ; ?>"
                >
                <?php if (isset($errors['middle_name']['invalidFormat'])) :?>
                    <small class="text-danger"><?= h($errors['middle_name']['invalidFormat']) ?></small>
                <?php else :?>
                    <small class="text-muted"><?= __('Enter your middle name (optional).') ?></small>
                <?php endif ;?>
            </div>
            <div class="mb-3">
                <input type="text" name="last_name" class="form-control" placeholder="<?= __('Last Name') ?>" 
                    value="<?= (isset($form['last_name'])) ?  h($form['last_name']) : '' ; ?>"
                >
                <?php if (isset($errors['last_name']['_empty'])) :?>
                    <small class="text-danger"><?= h($errors['last_name']['_empty']) ?></small>
                <?php elseif (isset($errors['last_name']['invalidFormat'])) :?>
                    <small class="text-danger"><?= h($errors['last_name']['invalidFormat']) ?></small>
                <?php else :?>
                    <small class="text-muted"><?= __('Enter your last name.') ?></small>
                <?php endif ;?>
            </div>
            <div class="mb-3">
                <input type="email" name="email_address" class="form-control" placeholder="<?= __('Email Address') ?>" 
                    value="<?= (isset($form['email_address'])) ?  h($form['email_address']) : '' ; ?>"
                >
                <?php if (isset($errors['email_address']['_empty'])) :?>
                    <small class="text-danger"><?= h($errors['email_address']['_empty']) ?></small>
                <?php elseif (isset($errors['email_address']['email'])) :?>
                    <small class="text-danger"><?= h($errors['email_address']['email']) ?></small>
                <?php else :?>
                    <small class="text-muted"><?= __('Enter your email address.') ?></small>
                <?php endif ;?>
            </div>
            <div class="mb-3">
                <input 
                    type="date" 
                    name="birthdate" 
                    class="form-control"
                    value="<?= (isset($form['birthdate'])) ?  h($form['birthdate']) : '' ; ?>"
                >
                <?php if (isset($errors['birthdate']['_empty'])) :?>
                    <small class="text-danger"><?= h($errors['birthdate']['_empty']) ?></small>
                <?php elseif (isset($errors['birthdate']['date'])) :?>
                    <small class="text-danger"><?= h($errors['birthdate']['date']) ?></small>
                <?php else :?>
                    <small class="text-muted"><?= __('Enter your birth date.') ?></small>
                <?php endif ;?>
            </div>
            <div class="mb-3">
                <input 
                    type="password" 
                    minlength="8" 
                    name="password" 
                    class="form-control" 
                    placeholder="<?= __('Password') ?>"
                    value="<?= (isset($form['password'])) ?  h($form['password']) : '' ; ?>"
                >
                <?php if (isset($errors['password']['minLength'])) :?>
                    <small class="text-danger"><?= h($errors['password']['minLength']) ?></small>
                <?php elseif (isset($errors['password']['_empty'])) :?>
                    <small class="text-danger"><?= h($errors['password']['_empty']) ?></small>
                <?php else :?>
                    <small class="text-muted"><?= __('Enter your desired password.') ?></small>
                <?php endif ;?>
            </div>
            <div class="mb-3">
                <input 
                    type="password" 
                    minlength="8" 
                    id="inputPassword" 
                    name="repeatPassword" 
                    class="form-control" 
                    placeholder="<?= __('Repeat Password') ?>"
                    value=""
                >
                <small class="text-muted"><?= __('Repeat the password that you entered.') ?></small>
            </div>
            <div class="text-right">
                <a href="<?= $this->Url->build('/', true) ?>" class="btn btn-secondary"><?= __('Cancel') ?></a>
                <button type="submit" class="btn btn-primary"><?= __('Register') ?></button>
            </div>
        </form>
    </div>
</div>
</div>


