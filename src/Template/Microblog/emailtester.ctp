<div class="container-fluid">
    <div class="card microblog-view">
        <div class="card-body">
            <h4>Email Tester</h4>
            <p>Test emails.</p>
            <form method="POST">
                <div class="mb-2">
                    <input class="form-control" 
                    type="text" 
                    name="inputEmailAddress" 
                    placeholder="Your email address"
                    value="<?= (isset($email_address)) ? $email_address : '' ?>"
                    required>
                    <small class="text-muted">Enter the email address here.</small>                    
                </div>
                <div class="text-right">
                    <button class="btn btn-primary" type="submit">Send</button>
                </div>
            </form>        
        </div>
    </div>
</div>