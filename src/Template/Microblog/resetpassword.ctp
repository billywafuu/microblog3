<?= $this->Flash->render('cp_error') ?> 
<?= $this->Flash->render('cp_success') ?>    
<div class="container">
    <div class="card mt-2 mx-auto" id="changePasswordCard">
        <div class="card-body">
            <div class="container-fluid">
                <h4><?= __('Change Password') ?></h4>
                <p><?= __('You can change your account password from here. 
                We suggest to pick a password that has more than 8 characters with a combination of letters and numbers for maximum security.') ?></p>
                <form method="POST" id="changePasswordForm">
                    <div class="mb-2">
                        <input class="form-control" type="password" id="inputPassword" name="inputPassword" minlength="8" required>
                        <small class="text-muted"><?= __('Enter a new password.') ?></small>                    
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="password" id="inputPassword2" name="inputPassword2" minlength="8" required>
                        <small class="text-muted"><?= __('Repeat the new password that you entered.') ?></small>                    
                    </div>
                    <div class="alert alert-warning alert-dismissible fade show" id="changePasswordAlert" role="alert">
                        <div id="changePasswordAlertBody"></div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>                
                    <div class="text-right">
                        <button type="button" onclick="validateChangePassword()" class="btn btn-primary"><i class="fas fa-save"></i> <?= __('Save Changes') ?></button>
                    </div>
                </form>           
            </div>
        </div>
    </div>
</div>  
<script> 
    $(document).ready(()=>{

        // Hide the alert on document ready.
        $("#changePasswordAlert").hide();

    })

    // A function that validates the passwords before
    // firing the form submit. 
    function validateChangePassword(){

        // Hide the alert on function execution. 
        $("#changePasswordAlert").hide();

        // Get the 2 new passwords. 
        newPassword     = g('inputPassword').value;
        repeatPassword  = g('inputPassword2').value;

        // Check empties. 
        if(newPassword.length == 0 || repeatPassword.length == 0){
            g('changePasswordAlertBody').innerHTML = "<strong>Please fill out the fields</strong><br> All the fields are required to change your password.";
            $("#changePasswordAlert").show();
            return; 
        }

        // Check if it does not match. Notify the user to re-enter. 
        if(newPassword != repeatPassword){
            g('changePasswordAlertBody').innerHTML = "<strong>Password Mismatch</strong><br> The new password and the repeat password does not match. Please re-enter again.";
            $("#changePasswordAlert").show();
            return; 
        }

        // Check the length. 
        if(newPassword.length < 8){
            g('changePasswordAlertBody').innerHTML = "<strong>Password too short</strong><br> Please enter a new password that has 8 characters or more.";
            $("#changePasswordAlert").show();
            return;            
        }

        // If everything is cleared on the client side. Fire the form.
        g('changePasswordForm').submit();

    }
</script>