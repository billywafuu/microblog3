<?php if(!$this->Session->check('Auth.User.id')): ?>
    <?php echo $this->element('jumbotron') ?>
    <?php echo $this->element('login_form') ?>
<?php else: ?>
    <?php echo $this->element('home_page') ?>
<?php endif; ?>