<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= $this->request->webroot ?>/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= $this->request->webroot ?>/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= $this->request->webroot?>/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= $this->request->webroot ?>/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= $this->request->webroot ?>/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= $this->request->webroot ?>/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= $this->request->webroot?>/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= $this->request->webroot ?>/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= $this->request->webroot ?>/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->request->webroot ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= $this->request->webroot ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= $this->request->webroot ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= $this->request->webroot?>/favicon-16x16.png">
	<link rel="manifest" href="<?= $this->request->webroot ?>/manifest.json">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= $this->request->webroot ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">   
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
	<?= $this->Html->css('microblog.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('js') ?>
    <script type="text/javascript" src="<?= $this->request->webroot?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot?>js/popper.min.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot?>js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script src="<?= $this->request->webroot ?>js/debounce.min.js"></script>
	<?php if($this->Session->check('Auth.User.id')):?>
		<?php echo $this->element('Javascripts/post')?>
		<?php echo $this->element('Javascripts/like')?>
		<?php echo $this->element('Javascripts/comment')?>
		<?php echo $this->element('Javascripts/follow')?>
	<?php endif;?>
</head>
<body>
	<div id="header">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="main-navbar">
			<a class="navbar-brand" href="<?= $this->Url->build('/', true)?>">
				<img src="<?= $this->request->webroot ?>img/logo.jpg" 
					width="30" 
					height="30" 
					class="d-inline-block align-top" 
					alt=""
				>
  			</a>
			<?php if($this->Session->check('Auth.User.id')) :?>
				<button class="navbar-toggler" type="button" data-toggle="collapse" 
				data-target="#navbarNav" 
				aria-controls="navbarNav" 
				aria-expanded="false" 
				aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<form class="form-inline my-2 my-lg-0" action="<?= $this->Url->build('/', true) ?>search">
					<div class="input-group">
						<input type="text" class="form-control" name="q" id="searchQuery" placeholder="Search Microblog" 
						value="">
						<div class="input-group-append">
							<button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
						</div>
					</div>
					</form>
					<div class="mobile-only">
						<?php echo $this->element('Navbar/user_dropdown')?>
					</div>
				</div>
				<div class="d-none d-lg-block mr-4">
					<a type="button" href="http://<?=$_SERVER['SERVER_NAME'] ?><?= $this->request->webroot?>" 
					class="btn btn-lg btn-primary mr-2"> 
						<i class="fas fa-home"></i>
					</a>
				</div>
				<div class="d-none d-lg-block mr-4">
					<a type="button" href="<?= $this->Url->build('/', true) ?>posts/create" 
					class="btn btn-md btn-success mr-2"> 
						<i class="fas fa-plus"></i> Post
					</a>
				</div>
				<div class="d-none d-lg-block mr-4">
					<?php echo $this->element('Navbar/user_dropdown')?>
				</div>
			<?php endif;?>
		</nav>	
	</div>
    <?= $this->Flash->render() ?>
        <div class="container clearfix">
            <?= $this->fetch('content') ?>
        </div>
</body>
<script>
		var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
		function g(id){ return document.getElementById(id) }
		function bu(){return '<?= $this->request->webroot ?>'}
		$(document).ready(() => {
			var mainNavbar = $("#main-navbar");
			$(document).scroll($.debounce(500, function(e) {
				if(window.pageYOffset > 300){
					$(mainNavbar).addClass("fixed-top");
				}else{ 
					$(mainNavbar).removeClass("fixed-top");
				}
			}));    
			console.log("Welcome to " + bu() + ".");
		})
	</script>
</html>
