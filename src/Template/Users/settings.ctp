<?= $this->element('croppie') ?>
<?php
    $filename = $this->request->webroot
    . 'user/' . $currentUser['User']['id'] 
    . '.' . $currentUser['User']['image_file_type'];
?>
<?php echo $this->Flash->render('save_success') ?>
<div class="container">
    <div class="card mt-2 mx-auto microblog-view" id="settingsCard">
        <div class="card-body">
        <h5><?= $this->element('back_arrow')?><?= __('Settings') ?> </h5>
            <div class="container-fluid mb-3 text-center">
                <div style="width:150px; height:150px" class="mx-auto">
                    <img 
                    src="<?= $filename . '?' . date('Y-m-d H:i:s') ?>" 
                    class="img-fluid rounded-circle"
                    onerror="this.src='https://cdn.pixabay.com/photo/2018/11/13/21/43/instagram-3814049_960_720.png'"
                    >
                </div>
                <button class="btn btn-sm btn-light mt-4" onclick="$('#photo_upload').modal('show')">
                    <i class="fas fa-upload"></i><?= __(' Change Profile Photo') ?>
                </button>
            </div>
            <div class="container-fluid">
                <form method="POST" id="settingsForm">
                    <div class="mb-2">
                        <input class="form-control" type="email" id="inputEmailAddress" name="email_address" 
                        value="<?= ($currentUser['User']['email_address']) ? $currentUser['User']['email_address']  : '' ; ?>">
                        <?php if (isset($errors['email_address']['_empty'])) :?>
                            <small class="text-danger"><?= h($errors['email_address']['_empty']) ?></small>
                        <?php elseif (isset($errors['email_address']['email'])) :?>
                            <small class="text-danger"><?= h($errors['email_address']['email']) ?></small>
                        <?php else :?>
                            <small class="text-muted"><?= __('Enter your email address.') ?></small>
                        <?php endif ;?>                
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="text"  name="first_name" 
                        value="<?= ($currentUser['User']['first_name']) ? $currentUser['User']['first_name']  : '' ; ?>">
                        <?php if (isset($errors['first_name']['_empty'])) :?>
                            <small class="text-danger"><?= h($errors['first_name']['_empty']) ?></small>
                        <?php elseif (isset($errors['first_name']['invalidFormat'])) :?>
                            <small class="text-danger"><?= h($errors['first_name']['invalidFormat']) ?></small>
                        <?php else :?>
                            <small class="text-muted"><?= __('Enter your first name.') ?></small>
                        <?php endif ;?>                
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="text"  name="middle_name" 
                        value="<?= ($currentUser['User']['middle_name']) ? $currentUser['User']['middle_name']  : '' ; ?>">
                        <?php if (isset($errors['middle_name']['invalidFormat'])) :?>
                            <small class="text-danger"><?= h($errors['middle_name']['invalidFormat']) ?></small>
                        <?php else :?>
                            <small class="text-muted"><?= __('Enter your middle name (optional).') ?></small>
                        <?php endif ;?>               
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="text"  name="last_name" 
                        value="<?= ($currentUser['User']['last_name']) ? $currentUser['User']['last_name']  : '' ; ?>">
                        <?php if (isset($errors['last_name']['_empty'])) :?>
                            <small class="text-danger"><?= h($errors['last_name']['_empty']) ?></small>
                        <?php elseif (isset($errors['last_name']['invalidFormat'])) :?>
                            <small class="text-danger"><?= h($errors['last_name']['invalidFormat']) ?></small>
                        <?php else :?>
                            <small class="text-muted"><?= __('Enter your last name.') ?></small>
                        <?php endif ;?>                 
                    </div>
                    <div class="mb-2">
                        <input class="form-control" type="date"  name="birthdate" 
                        value="<?= ($currentUser['User']['birthdate']) ? $currentUser['User']['birthdate']->format('Y-m-d')  : '' ; ?>">
                        <?php if (isset($errors['birthdate']['_empty'])) :?>
                            <small class="text-danger"><?= h($errors['birthdate']['_empty']) ?></small>
                        <?php elseif (isset($errors['birthdate']['date'])) :?>
                            <small class="text-danger"><?= h($errors['birthdate']['date']) ?></small>
                        <?php else :?>
                            <small class="text-muted"><?= __('Enter your birth date.') ?></small>
                        <?php endif ;?>              
                    </div>
                    <div class="text-right">
                        <button type="button" onclick="saveForm()" class="btn btn-primary"><i class="fas fa-save"></i> <?= __('Save Changes') ?> </button>
                    </div>
                </form>           
            </div>

        </div>
    </div>
</div>

<script> 
    var currentEmailAddress = '<?= ($currentUser['User']['email_address']) ? h($currentUser['User']['email_address'])  : '' ; ?>';
    function saveForm(){
        if(currentEmailAddress != "" && currentEmailAddress != g('inputEmailAddress').value){
            $.confirm(
                {
                    title: '<?= __('Change Email Address?') ?>',
                    content: '<?= __('When you change your email address for this account, you need to activate it again.') ?>' +
                    '<br><br><small class="text-muted"><?= __('We will be sending a new activation link to the new email address that you entered.') ?></small><br><hr>Continue?',
                    buttons: { 
                        confirm: { 
                            text: '<?= __('Yes') ?>',
                            btnClass: 'btn-success',
                            action: () => {
                                g('settingsForm').submit();
                            }
                        },
                        cancel: {
                            text: '<?= __('No') ?>'
                        }
                    }
                }
            );
        }else{
            g('settingsForm').submit();
        }
       
    }
</script>