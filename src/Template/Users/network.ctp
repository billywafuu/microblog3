<div class="container-fluid">
    <div class="container">
        <div class="card microblog-view">
            <div class="card-body">
                <h4><?= $this->element('back_arrow')?> <?= __('My Network') ?></h4>
            <hr>
            <div class="form-group">
                <input type="email" class="form-control" id="inputNetworkSearch" placeholder="Search your network..">
                <small id="emailHelp" class="form-text text-muted"><?= __('A network is a set of people that is either following you or you are a follower of them.') ?></small>
            </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="followings-tab" onclick="changeView('followings')" data-toggle="tab" href="#followings" role="tab" aria-controls="followings" aria-selected="true">
                            <i class="fas fa-running"></i><?= __('Following') ?>  <span class="badge badge-light" id="followings-count-badge"><?= $viewData['followingsCount'] ?></span>
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="followers-tab" onclick="changeView('followers')" data-toggle="tab" href="#followers" role="tab" aria-controls="followers" aria-selected="false">
                            <i class="fas fa-users"></i><?= __('Followers') ?>  <span class="badge badge-light" id="followers-count-badge"><?= $viewData['followersCount'] ?></span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="followings" role="tabpanel" aria-labelledby="followings-tab">
                        <div class="container text-center p-4" id="followings-spinner">
                            <div class="spinner-border text-primary p-4 mb-4" role="status">
                                <span class="sr-only"><?= __('Loading..') ?></span>
                            </div>  
                        </div>
 
                        <div class="container-fluid" id="followings-container">
             
                        </div>
                        <div class="container-fluid text-center" id="followings-pagination">
                        
                        </div>
                    </div>
                    <div class="tab-pane fade" id="followers" role="tabpanel" aria-labelledby="followers-tab">
                        <div class="container text-center p-4" id="followers-spinner">
                            <div class="spinner-border text-primary p-4 mb-4" role="status">
                                <span class="sr-only"><?= __('Loading..') ?></span>
                            </div>  
                        </div> 

                        <div class="container-fluid" id="followers-container">
                        
                        </div>
                        <div class="container-fluid text-center" id="followers-pagination">
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  

<script>
    var currentView = "followings";
    var followingsSpinner;
    var followersSpinner;
    var followingsContainer;
    var followersContainer;
    var followingsPagination;
    var followersPagination
    $(document).ready(()=>{
        followingsSpinner    = $("#followings-spinner");
        followersSpinner     = $("#followers-spinner");
        followingsContainer  = $("#followings-container");
        followingsPagination = $("#followings-pagination");
        followersPagination  = $("#followers-pagination");
        followersContainer   = $("#followers-container");

        $(followingsSpinner).hide();
        $(followersSpinner).hide();

        // Hook up a keyup event that fires every 500 ms instead on every key up.
        $("#inputNetworkSearch").keyup($.debounce(500, function(e) {
            loadNetwork(1);
        }));  

        loadNetwork(1);

    });
    function loadNetwork(page){
        $.ajax({
            url: '<?= $this->request->webroot ?>users/network',
            data: { 
                search: $("#inputNetworkSearch").val(), 
                page: page,
                view: currentView
            },
            datatype: 'json',
            type:'post',
            beforeSend: () => {

                $(followingsSpinner).show();
                $(followersSpinner).show();
                $(followingsContainer).html('');
                $(followersContainer).html('');

            },
            success: (response)=> {
                response = JSON.parse(response);

                $(followingsSpinner).hide();
                $(followersSpinner).hide();
                
                if(response.result == "success"){

                    if(response.view == "followings"){
                        $(followingsContainer).html(response.html);
                        $(followingsPagination).html(response.pagination);
                    }

                    if(response.view == "followers"){
                        $(followersContainer).html(response.html);
                        $(followersPagination).html(response.pagination);
                    }

                }

            },
            error: ()=> {
                $.dialog('<?= __('There has been an error while trying to retrieve your network.') ?>', 
                "My Network",);
                return;
            }

        });
    }
    function changeView(view){ 
        currentView = view;
        loadNetwork(1, currentView);
    }
    
</script>               