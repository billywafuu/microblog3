<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Model\Entity\User;
use App\Model\Entity\Activation;
use App\Model\Validation\UserUpdateValidator;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Cake\View\View;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 * @package Microblog.Controller
 *
 */
class UsersController extends AppController
{
    private $Session;
    /**
     * Initialize function.
     *
     * @return void
     *
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Rangen');
        $this->loadComponent('Posts');
        $this->loadComponent('Account');
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Session = $this->getRequest()->getSession();
    }
    /**
    * The profile action that renders the profile
    * view.
    *
    */
    public function profile()
    {
        $user = $this->Users->get($this->Auth->user('id'));
        $this->set('userProfile', array('User' => $user));
        $this->set('Followers', $user->getFollowers());
        $this->set('Followings', $user->getFollowings());
        $page = (isset($this->request->query['page'])) ? $this->request->query['page'] : 1;
        $posts = $this->Posts->getProfilePosts(
            $this->Auth->user('id'),
            $page,
            MAX_POSTS_PER_PAGE
        );
        if ($posts === null) {
            $this->set('posts', null);
            $this->set('pagination', null);
            return;
        }
        $this->set('posts', $posts['posts']);
        $this->set('pagination', $posts['pagination']);
    }
    /**
    * The settings action that renders the settings
    * view.
    *
    */
    public function settings()
    {
        if ($this->request->is('post')) {
            $x = new UserUpdateValidator();
            $errors = $x->errors($this->request->getData());
            if ($errors) {
                $this->Flash->warning(
                    __('There are some errors on your form. Please check and try again.')
                );
                $this->set('errors', $errors);
                return;
            }
            $result = $this->Account->update(
                $this->Auth->user('id'),
                $this->request->getData()
            );

            if ($result == 2) {
                $this->Flash->error(
                    __('Please enter a valid birth date.')
                );
                return;
            }

            if ($result == 0) {
                $this->Flash->error(
                    __('There are some errors on your form. Please check and try again.')
                );
                return;
            }
            $email_address = $this->request->getData('email_address');
            if ($email_address == $this->Auth->user('email_address')) {
                $this->Flash->success(
                    __('Your profile has been successfully updated.')
                );
                $this->redirect(
                    ['controller' => 'Users', 'action' => 'settings']
                );
                return;
            }
            $result = $this->Account->updateEmailAddress(
                $this->Auth->user('id'),
                $email_address
            );

            if ($result == 2) {
                $this->Flash->error(
                    __('The email address that you are trying to change is not available.')
                );
                return;
            }
            if ($result == 0) {
                $this->Flash->error(
                    __('We encountered an unexpected error. Please try again.')
                );
                return;
            }

            $this->Flash->success(
                'An email has been sent to ' . $email_address .
                ' to activate your account. Please check your email inbox.'
            );
            $this->redirect(
                ['controller' => '/', 'action' => 'logout']
            );
            return;
        }
    }
    /**
    * The network action that renders the network
    * view.
    *
    */
    public function network($id = null)
    {
        if ($id === null) {
            $id = $this->Auth->user('id');
        }
        $user = $this->Users->get(
            $id
        );

        $followers = $user->getFollowers();
        $followings = $user->getFollowings();
        $this->set(
            'viewData',
            array(
                'followingsCount' => count($followings),
                'followersCount'  => count($followers)
            )
        );
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $request = $this->request->getData();
            if (empty($request)) {
                return;
            }
            $Views = new View($this->request);
            $returnType = "followings";
            $data = null;
            if ($request['view'] == "followings") {
                $data
                    = $user->getFollowings($request['search'])->toArray();
            } else {
                $data
                    = $user->getFollowers($request['search'])->toArray();
            }

            if (count($data) == 0) {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "success",
                            "description" => "The request returned empty.",
                            "html" => $Views->element('Network/no_followers'),
                            "view" => $request['view'],
                            "count" => 0,
                            "pagination" => ""
                        )
                    )
                );
                return $this->response;
            }
            $currentPage = 1;
            if (isset($request['page'])) {
                $currentPage = $request['page'];
            }
            $result = $this->Paging->execute(
                $data,
                $currentPage,
                20
            );

            $html = null;
            $pagination = null;
            if ($request['view'] == "followings") {
                foreach ($result['result'] as $user) {
                    $html = $html . $Views->element(
                        'Network/user',
                        array(
                            "user" => $user->following
                        )
                    );
                }
            } else {
                foreach ($result['result'] as $user) {
                    $html = $html . $Views->element(
                        'Network/user',
                        array(
                            "user" => $user->user
                        )
                    );
                }
            }

            $result['pagination']['function'] = 'loadNetwork';
            $pagination = $Views->element(
                'js_pagination',
                $result['pagination']
            );
            $this->response->body(
                json_encode(
                    array(
                        "result" => "success",
                        "description" => "The request was successful.",
                        "view" => $request['view'],
                        "overallCount" => $result['pagination']['overallCount'],
                        "count" => $result['pagination']['count'],
                        "html" => $html,
                        "pagination" => $pagination
                    )
                )
            );
            return $this->response;
        }
    }
    /**
    * Views a user profile.
    *
    * @param int $id The id of the user to be viewed.
    * @return void
    */
    public function view($id)
    {
        if (!$this->Account->exists($id)) {
            $this->Flash->error(
                __('The account that you are trying to access was not found.')
            );
            return;
        }
        $user = $this->Users->get($id);
        $this->set('userProfile', array('User' => $user->toArray()));
        $this->set('Followers', $user->getFollowers());
        $this->set('Followings', $user->getFollowings());
        $page = (isset($this->request->query['page'])) ? $this->request->query['page'] : 1;
        $posts = $this->Posts->getProfilePosts(
            $id,
            $page,
            MAX_POSTS_PER_PAGE
        );
        if ($posts === null) {
            $this->set('posts', null);
            $this->set('pagination', null);
            return;
        }
        $this->set('posts', $posts['posts']);
        $this->set('pagination', $posts['pagination']);
    }
    /**
    * Renders a page where user can change their password.
    *
    * @return void
    */
    public function changePassword()
    {
        if ($this->request->is('post')) {
            $currentPassword = $this->request->data['inputCurrentPassword'];
            $newPassword     = $this->request->data['inputPassword'];

            if ($currentPassword == $newPassword) {
                $this->Flash->warning(
                    __('You cannot use your current password as your new password. 
                    Please create a new and unique password.')
                );
                return;
            }

            $result = $this->Account->changePassword(
                $this->Auth->user('id'),
                $currentPassword,
                $newPassword
            );

            if ($result == 2) {
                $this->Flash->error(
                    __('The current password that you entered is incorrect. 
                Please enter your current password to change your password')
                );
                return;
            }

            if ($result == 0) {
                $this->Flash->error(
                    __('
                        There has been error while changing your password.
                        Please try again.')
                );
                return;
            }

            $session = $this->getRequest()->getSession();
            $session->destroy();
            $this->Flash->success(
                __('You have successfully changed your password. Please login again.')
            );
            $this->redirect(
                ['controller' => 'Microblog', 'action' => 'login']
            );
        }
    }
    /**
    * Receives an AJAX request to upload new profile picture
    * for the current account. This action receives a $_FILE object
    * or a base64 data of a cropped circular image.
    */
    public function uploadImage()
    {
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            $this->redirect(
                ['controller' => 'Users', 'action' => 'settings']
            );
            return;
        }

        $request = $this->request->getData();
        if (empty($request)) {
            return [
                'result' => 'failed',
                'description' => 'No data sent by the client. Uploading aborted.'
            ];
        }

        if ($request['uploadAction'] == 'crop_existing') {
            $response = $this->Account->cropExisting(
                $this->Auth->user('id'),
                $request['fileToUpload']
            );
            $this->response->body(
                json_encode($response)
            );
            return $this->response;
        } else {
            $response = $this->Account->uploadImage(
                $this->Auth->user('id'),
                $_FILES['fileToUpload']
            );
            $this->response->body(
                json_encode($response)
            );
            return $this->response;
        }
    }
}
