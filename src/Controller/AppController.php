<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    private $usersTable;
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Paging');
        $this->loadComponent('Flash');
        $this->loadComponent('Lang');
        $this->loadComponent(
            'Auth',
            [
                'loginAction' => [
                    'controller' => '/',
                    'action' => 'login'
                ],
                'authError' => 'You must first log in your account.',
                'storage' => 'Session'
            ]
        );
        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3/en/controllers/components/security.html
         */
        $this->usersTable = TableRegistry::getTableLocator()->get('Users');
    }
    public function beforeFilter(Event $event)
    {
        $session = $this->getRequest()->getSession();
        if (!$session->check('locale')) {
            $session->write('locale', 'en_US');
            I18n::setLocale('en_US');
        } else {
            I18n::setLocale($session->read('locale'));
        }
        if ($this->Auth->user('id')) {
            $user = $this->usersTable->get($this->Auth->user('id'));
            if (empty($user)) {
                $this->Auth->logout();
                $this->redirect(
                    [
                        'controller' => '/',
                        'action' => 'logout'
                    ]
                );
            }
            $this->set(
                'currentUser',
                array("User" => $this->usersTable->get($this->Auth->user('id')))
            );
        }
    }
}
