<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use App\Model\Entity\Follow;
use Cake\ORM\TableRegistry;
use Cake\View\View;

/**
 * Comments Controller
 *
 * This controller handles all follow/unfollow related operations.
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 * @package Microblog.Controller
 */
class FollowsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    /**
     * Follows a certain user. This action receives the user_id of the user that will
     * be followed.
     *
     * @return object Returns a JSON object that containing the result.
     */
    public function follow()
    {
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            $this->redirect(
                ['controller' => '/', 'action' => 'signin']
            );
            return;
        }
        $user_id = $this->request->getData('user_id');
        $users = TableRegistry::getTableLocator()->get('Users');
        $user = $users->get($user_id);
        if (empty($user)) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" => "No user found with that id."
                    )
                )
            );
            return $this->response;
        }
        if ($user->status == 0) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" => "The user needs activation first."
                    )
                )
            );
            return $this->response;
        }
        $follow = $this->Follows->find()->
        where(
            [
                'user_id' => $this->Auth->user('id'),
                'following_id' => $user->id,
                'date_unfollowed IS NULL'
            ]
        )->first();

        if (empty($follow)) {
            $newFollow = new Follow();
            $newFollow->user_id = $this->Auth->user('id');
            $newFollow->following_id = $user->id;
            if ($this->Follows->save($newFollow)) {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "follow_success",
                            "description" => "The follow was successful.",
                            "userName" => $user->first_name . ' '
                            . $user->last_name
                        )
                    )
                );
                return $this->response;
            }
        } else {
            $follow->date_unfollowed = date('Y-m-d H:i:s');
            if ($this->Follows->save($follow)) {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "unfollow_success",
                            "description" => "The unfollow was successful.",
                            "userName" => $user->first_name . ' '
                            . $user->last_name
                        )
                    )
                );
                return $this->response;
            }
        }
    }
}
