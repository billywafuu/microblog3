<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use App\Model\Entity\Like;
use App\Model\Validation\PostValidator;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Likes Controller
 *
 * This controller handles all Likes related operations.
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 * @package Microblog.Controller
 */
class LikesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    /**
     * Like/unlike a certain post. This action receives a post_id as
     * the payload.
     *
     * @return object Returns a JSON object that contains the result.
     */
    public function like()
    {
        if (!$this->request->is('ajax')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "invalid_request",
                        "description" =>
                        "The request was invalid and cannot be processed."
                    )
                )
            );
            return $this->response;
        }

        if (!$this->Auth->user('id')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "You must be logged in."
                    )
                )
            );
            return $this->response;
        }

        $posts = TableRegistry::getTableLocator()->get('Posts');
        $post = $posts->find('all')
            ->where(
                [
                    'id' => $this->request->getData('post_id')
                ]
            )
            ->contain(
                [
                    'Likes'
                ]
            );
        $post = $post->first();

        if (!$post) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The post that you are trying to like/unlike was not found."
                    )
                )
            );
            return $this->response;
        }
        if ($post->deleted == 1) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The post that you are trying to like/unlike was not found."
                    )
                )
            );
            return $this->response;
        }

        foreach ($post->likes as $like) {
            if ($like->user_id == $this->Auth->user('id')) {
                $like->deleted = 1;
                $like->deleted_at = date('Y-m-d H:i:s');
                if (!$this->Likes->save($like)) {
                    $this->response->body(
                        json_encode(
                            array(
                                "result" => "failed",
                                "description" =>
                                "There was an error while trying to unlike the post."
                            )
                        )
                    );
                    return $this->response;
                }
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "success",
                            "state" => "unliked",
                            "description" =>
                            "Successfully unliked this post."
                        )
                    )
                );
                return $this->response;
            }
        }

        $newLike = new Like();
        $newLike->user_id = $this->Auth->user('id');
        $newLike->post_id = $post->id;
        $newLike->created = date('Y-m-d H:i:s');

        if (!$this->Likes->save($newLike)) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "There was an error while trying to like/unlike the post."
                    )
                )
            );
            return $this->response;
        }

        $this->response->body(
            json_encode(
                array(
                    "result" => "success",
                    "state" => "liked",
                    "description" =>
                    "Successfully liked this post."
                )
            )
        );
        return $this->response;
    }
}
