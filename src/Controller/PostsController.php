<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use App\Model\Entity\Post;
use App\Model\Validation\PostValidator;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Posts Controller
 *
 * This controller handles all user post related operations.
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 * @package Microblog.Controller
 *
 */
class PostsController extends AppController
{
    public $components = ['Upload'];
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    /**
    * A controller action that is called when creating
    * new posts.
    * @return void
    */
    public function create()
    {
        if ($this->request->is('post')) {
            $validator = new PostValidator();
            $errors = $validator->errors($this->request->getData());

            if ($errors) {
                $this->Flash->warning(
                    __('There are some errors on your form. Please check and try again.')
                );
                $this->set('errors', $errors);
                $this->set('defaultFields', $this->request->getData());
                return;
            }

            $post = new Post();
            $post->user_id = $this->Auth->user('id');
            $post->title   = $this->request->getData('title');
            $post->body    = nl2br($this->request->getData('body'));
            $post->created = date('Y-m-d H:i:s');
            $post->post_id = null;
            if (!$this->Posts->save($post)) {
                $this->Flash->error('Failed to create post. Please try again.');
                return;
            }
            if (empty($_FILES['postImage']['name'])) {
                if ($post->title == "" && $post->body == "") {
                    $this->Flash->error('There should be at least a title, a body or an image to share a post.');
                    return;
                }
                $this->Flash->success(
                    'The post has been successfully created.'
                );

                $this->redirect(
                    ['controller' => 'Posts','action' => 'view', $post->id]
                );
                return;
            }
            $post->image_file_type = $this->Upload->getFileType($_FILES['postImage']);
            $result = $this->Upload->file(
                $_FILES['postImage'],
                'post',
                $post->id . '.' . $post->image_file_type
            );

            if ($result == 2) {
                $this->Flash->warning('You can only upload .jpg, .png and .jpeg files for this post.');
                return;
            }
            if ($result == 0) {
                $this->Flash->warning(
                    'There has been a problem while trying to upload the image.
                    Please try again.'
                );
                return;
            }
            if (!$this->Posts->save($post)) {
                $this->Flash->error('Failed to create post. Please try again.');
                return;
            }
            $this->Flash->success(
                'The post has been successfully created.'
            );
            $this->redirect(
                ['controller' => 'Posts','action' => 'view', $post->id]
            );
        }
    }
    /**
    * Views a post.
    *
    * @param string|null $id The id of the post to view.
    */
    public function view($id = null)
    {
        if ($id === null) {
            $this->render('not_found');
            return;
        }
        $query = $this->Posts->find('all')
            ->where(
                [
                    'Posts.id ' => $id,
                    'Posts.deleted' => 0
                ]
            )
            ->contain(
                [
                    'Owner',
                    'Retweet',
                    'Likes'
                ]
            );
        $result = $query->first();
        if (!$result) {
            $this->render('not_found');
            return;
        }

        $return = array(
            'Post' => $result->toArray(),
            'Owner' => $result['owner']->toArray(),
            'Likes' => $result['likes'],
            'Retweet' => null
        );

        if ($result['retweet'] !== null) {
            $return['Retweet'] = $result['retweet']->toArray();
        }

        $this->set(
            'post',
            $return
        );
    }
    /**
    * Views a post.
    *
    * @param string|null $id The id of the post to edit.
    */
    public function edit($id = null)
    {
        if ($this->request->is('post')) {
            $post = $this->Posts->get($id);

            if (!$post) {
                $this->Flash->error(
                    'The post that you are trying to edit does not belong to you.'
                );
                $this->render('not_found');
                return;
            }

            if ($post->deleted == 1) {
                $this->Flash->error(
                    'The post that you are trying was deleted.'
                );
                $this->render('not_found');
                return;
            }

            if ($post->user_id != $this->Auth->user('id')) {
                $this->Flash->error(
                    'The post that you are trying to edit does not belong to you.'
                );
                $this->redirect(
                    ['controller' => 'Posts','action' => 'view', $id]
                );
                return;
            }

            $validator = new PostValidator();
            $errors = $validator->errors($this->request->getData());

            if ($errors) {
                $this->Flash->warning(
                    'There are errors on your edits. Please try again.'
                );
                $this->set('postData', array('Post' => $post->toArray()));
                return;
            }

            $post->title = $this->request->getData('title');
            $post->body = $this->request->getData('body');

            if (empty($_FILES['postImage']['name'])) {
                if (!$this->Posts->save($post)) {
                    $this->Flash->error(
                        "There has been an error while saving the post. Please try again."
                    );
                }
                $this->Flash->success('The post has been successfully updated.');
                $this->redirect(
                    ['controller' => 'Posts','action' => 'view', $id]
                );
                return;
            }

            if (!getimagesize($_FILES['postImage']["tmp_name"])) {
                $this->Flash->error(
                    "Please only upload image files for your post."
                );
                $this->set('postData', array('Post' => $post->toArray()));
                return;
            }

            $uploadPath = 'post/';

            $imageFileType = strtolower(
                pathinfo(
                    $uploadPath . basename($_FILES['postImage']["name"]),
                    PATHINFO_EXTENSION
                )
            );

            if ($imageFileType != "jpg"
                && $imageFileType != "png"
                && $imageFileType != "jpeg"
                && $imageFileType != "gif"
            ) {
                $this->Flash->error(
                    'You can only upload .jpg, .png and .jpeg files.'
                );
                $this->set('postData', array('Post' => $post->toArray()));
                return;
            }

            $fileName = $post->id . "."  . $imageFileType;
            $newFile = $uploadPath  . $fileName;

            if (file_exists($newFile)) {
                unlink($newFile);
            }

            if (!move_uploaded_file($_FILES['postImage']['tmp_name'], $uploadPath . $fileName)) {
                $this->Flash->error(
                    "There has been an error while uploading your image. Please try again."
                );
            }

            $post->image_file_type = $imageFileType;
            if (!$this->Posts->save($post)) {
                $this->Flash->error(
                    "There has been an error while saving the post. Please try again."
                );
            }

            $this->Flash->success('The post has been successfully updated.');
            $this->redirect(
                ['controller' => 'Posts','action' => 'view', $id]
            );
            return;
        }

        if ($id === null) {
            $this->render('not_found');
            return;
        }

        $post = $this->Posts->get($id);

        if (!$post) {
            $this->render('not_found');
            return;
        }

        if ($post->deleted == 1) {
            $this->render('not_found');
            return;
        }

        if ($post->user_id != $this->Auth->user('id')) {
            $this->Flash->error(
                'The post that you are trying to edit does not belong to you.'
            );
            $this->redirect(
                ['controller' => 'Posts','action' => 'view', $id]
            );
            return;
        }

        $this->set('postData', array('Post' => $post->toArray()));
    }
    /**
    * Retweets a post.
    *
    */
    public function retweet()
    {
        if (!$this->request->is('ajax')) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "failed",
                    "description" => "Invalid request."
                    )
                )
            );
            return $this->response;
        }

        $id = $this->request->getData('post_id');

        if (empty($id)) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "failed",
                    "description" => "No post id submitted to retweet."
                    )
                )
            );
            return $this->response;
        }

        $post = $this->Posts->get($id);

        if (empty($post)) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "post_not_found",
                    "description" => "Post does not exist."
                    )
                )
            );
            return $this->response;
        }

        if ($post->deleted) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "post_not_found",
                    "description" => "Post does not exist."
                    )
                )
            );
            return $this->response;
        }

        $newPost = new Post();
        $newPost->title = '@retweet';
        $newPost->body = nl2br($this->request->getData('input'));
        $newPost->user_id = $this->Auth->user('id');
        $newPost->post_id = $id;
        $newPost->created = date('Y-m-d H:i:s');

        if (!$this->Posts->save($newPost)) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "failed",
                    "description" => "The retweet has failed."
                    )
                )
            );
            return $this->response;
        }

        $this->response->body(
            json_encode(
                array(
                "result" => "retweet_success",
                "description" => "The post was successfully retweeted."
                )
            )
        );
        return $this->response;
    }
    /**
    * Deletes a post. This action only receives AJAX with a
    * payload of post_id.
    *
    */
    public function delete()
    {
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            $this->redirect(
                ['controller' => 'Users','action' => 'profile']
            );
            return;
        }
        $id = $this->request->getData('post_id');
        $post = $this->Posts->get($id);
        if (empty($post)) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "delete_failed",
                    "description" => "Post does not exist."
                    )
                )
            );
            return $this->response;
        }
        if ($post->deleted == 1) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "delete_failed",
                    "description" => "Post does not exist."
                    )
                )
            );
            return $this->response;
        }

        if ($post->user_id != $this->Auth->user('id')) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "unauthorized_operation",
                    "description" => "You 
                    do not own the post that you want to delete."
                    )
                )
            );
            return $this->response;
        }

        $post->deleted = 1;
        $post->deleted_at = date('Y-m-d H:i:s');
        if ($this->Posts->save($post)) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "delete_success",
                    "description" => "The post was successfully deleted."
                    )
                )
            );
            return $this->response;
        }
    }
}
