<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use App\Model\Entity\Comment;
use Cake\ORM\TableRegistry;
use Cake\View\View;

/**
 * Comments Controller
 *
 * This controller handles all user comment related operations.
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 * @package Microblog.Controller
 */
class CommentsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    /**
     * This action is responsible for posting new comments to
     * a certain post.
     *
     * This only acknowldeges AJAX post that takes a comment value and
     * and the id of the post.
     *
     * @return object Returns a JSON object that contains the comment, result and the
     * html.
     */
    public function post()
    {
        if (!$this->request->is('ajax')) {
            $this->redirect(
                ['controller' => '/', 'action' => 'index']
            );
            return;
        }

        $comment = $this->request->getData();
        if (empty($comment)) {
            return json_encode(
                array(
                    "result" => "failed",
                    "description" => "No comment data was sent to the server."
                )
            );
        }

        if (!$this->Auth->user('id')) {
            return json_encode(
                array(
                    "result" => "failed",
                    "description" => "A log in is required for this operation."
                )
            );
        }

        $posts = TableRegistry::getTableLocator()->get('Posts');
        $post = $posts->get($comment['post_id']);

        if (!$post) {
            return json_encode(
                array(
                    "result" => "failed",
                    "description" =>
                    "The post that you are trying to comment 
                    has been removed or does not exist."
                )
            );
        }

        $newComment = new Comment();
        $newComment->post_id = $comment['post_id'];
        $newComment->user_id = $this->Auth->user('id');
        $newComment->comment = nl2br($comment['comment']);
        $newComment->created = date("Y-m-d H:i:s");

        if (!$this->Comments->save($newComment)) {
            $this->response->body(
                array(
                    "result" => "failed",
                    "description" =>
                    "There was an error while trying to save the comment."
                )
            );
            return $this->response;
        }

        $comment['id'] = $newComment->id;
        $comment['user_id'] = $this->Auth->user('id');

        $view = new View($this->request);
        $users = TableRegistry::getTableLocator()->get('Users');

        $user = $users->get($this->Auth->user('id'));
        $this->autoRender = false;
        $this->response->body(
            json_encode(
                array(
                    "result" => "success",
                    "description" => "The comment was successfully posted.",
                    "html"  => $view->element(
                        'Comments/user_comment_template',
                        array(
                            'comment' => h($comment),
                            'user' => $user
                        )
                    )
                )
            )
        );
        return $this->response;
    }
    /**
     * Outputs comments as JSON object.
     *
     * @return object Returns a JSON object that contains the comment, result and the
     * html.
     */
    public function load()
    {
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "failed",
                    "description" => "Invalid request."
                    )
                )
            );
            return $this->response;
        }

        if (!$this->Auth->user('id')) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "failed",
                    "description" => "Invalid request."
                    )
                )
            );
            return $this->response;
        }

        $request = $this->request->getData();
        $posts = TableRegistry::getTableLocator()->get('Posts');
        $post = $posts->get($request['post_id']);

        if (empty($post)) {
            $this->response->body(
                json_encode(
                    array(
                    "result" => "failed",
                    "description" => "The post that you requested does not exist."
                    )
                )
            );
            return $this->response;
        }

        $overallComments = $this->Comments->find('all')
            ->where(
                [
                    'post_id' => $request['post_id'],
                    'Comments.deleted' => 0
                ]
            )->contain(
                ['Owner']
            )
            ->toArray();
        if (count($overallComments) == 0) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "success",
                        "description" => "There are no comments found on this post.",
                        "html" => ""
                    )
                )
            );
            return $this->response;
        }
        $Views = new View($this->request);
        $commentPages = ceil(count($overallComments) / MAX_COMMENTS_PER_POST);
        $currentPage = 1 ;
        $currentPage = $request['page'];
        if ($request['page'] < 0) {
            $currentPage = 0;
        }
        $offset = ($currentPage - 1 ) * MAX_COMMENTS_PER_POST;
        $limitComments = array_slice(
            $overallComments,
            $offset,
            MAX_COMMENTS_PER_POST
        );

        $html = "";
        foreach ($limitComments as $comment) {
            $html = $html . $Views->element(
                'Comments/user_comment_template',
                array(
                    "user" => $comment['owner'],
                    "comment" => $comment
                )
            );
        }

        $loadMoreComments = ($currentPage < $commentPages) ? true : false ;
        $this->response->body(
            json_encode(
                array(
                    "result" => "success",
                    "description" => "Comments retrieved.",
                    "count" => count($limitComments),
                    "loadMoreComments" => $loadMoreComments,
                    "html" => $html,
                    "offset" => $offset,
                    "page" => $request['page'],
                    "maxPage" => $commentPages
                )
            )
        );
        return $this->response;
    }
    /**
     * Deletes a comment. Only accepts AJAX request that has a post_id payload.
     *
     * @return response Returns a Cake response object that
     * contains the response params.
     */
    public function delete()
    {
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "invalid_request",
                        "description" =>
                        "The request was invalid and cannot be processed."
                    )
                )
            );
            return $this->response;
        }

        if (empty($this->request->data)) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" => "No data was sent."
                    )
                )
            );
            return $this->response;
        }

        if (!$this->Auth->user('id')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "You must be logged in to delete comments."
                    )
                )
            );
            return $this->response;
        }

        $id = filter_var($this->request->getData('id'), FILTER_VALIDATE_INT);
        $comment = $this->Comments->get($id);

        if (!$comment) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The comment was not found."
                    )
                )
            );
            return $this->response;
        }
        $posts = TableRegistry::getTableLocator()->get('Posts');
        $post = $posts->get($comment->post_id);

        if (!$post) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The post was not found."
                    )
                )
            );
            return $this->response;
        }

        if ($post->deleted == 1) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The post was not found."
                    )
                )
            );
            return $this->response;
        }
        if ($comment->user_id != $this->Auth->user('id')) {
            if ($post->user_id != $this->Auth->user('id')) {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "failed",
                            "description" =>
                            "You do not have the rights to delete this comment."
                        )
                    )
                );
                return $this->response;
            }
        }

        $comment->deleted = 1;
        $comment->deleted_at = date('Y-m-d H:i:s');

        if (!$this->Comments->save($comment)) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "Failed to delete comment. Please try again."
                    )
                )
            );
            return $this->response;
        }

        $this->response->body(
            json_encode(
                array(
                    "result" => "success",
                    "description" => "The comment was successfully deleted.",
                    "id" => $comment->id
                )
            )
        );
        return $this->response;
    }
    /**
     * Real-time retrieve a posts from the users that the current user
     * is following. This action receives AJAX with a timestamp and page as
     * payload.
     *
     */
    public function retrieve()
    {
        date_default_timezone_set('Asia/Manila');
        $this->autoRender = false;
        if (!$this->request->is('ajax')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "invalid_request",
                        "description" =>
                        "The request was invalid and cannot be processed."
                    )
                )
            );
            return $this->response;
        }

        if (!$this->Auth->user('id')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "You must be logged in first."
                    )
                )
            );
            return $this->response;
        }

        $posts = TableRegistry::getTableLocator()->get('Posts');
        $post = $posts->get($this->request->getData('post_id'));
        $timestamp = (int)$this->request->getData('timestamp');

        if (!$post) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The post was not found."
                    )
                )
            );
            return $this->response;
        }

        if ($post->deleted == 1) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "The post was not found."
                    )
                )
            );
            return $this->response;
        }

        $newComments = $this->Comments->find()->
        select(
            [
                'ts' => 'UNIX_TIMESTAMP(Comments.created)',
                'Owner.first_name',
                'Owner.last_name',
                'Owner.image_file_type',
                'Owner.id',
                'Comments.comment'
            ]
        )->where(
            [
                'post_id' => $post->id,
                'UNIX_TIMESTAMP(Comments.created) > ' . $timestamp
            ]
        )->contain(
            [
                'Owner'
            ]
        )->order(
            [
                'Comments.created' => 'DESC'
            ]
        )->limit(
            5
        )
        ->all();

        if (count($newComments) == 0) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "success",
                        "update" => "up_to_date",
                        "timestamp" => $timestamp
                    )
                )
            );
            return $this->response;
        }

        $html = "";
        $Views = new View($this->request);
        $latest_timestamp = 0;
        foreach ($newComments as $comment) {
            if ($latest_timestamp == 0) {
                $latest_timestamp = $comment->ts;
            }
            $html = $html . $Views->element(
                'Comments/user_comment_template',
                array(
                    "user" => $comment->owner,
                    "comment" => $comment
                )
            );
        }

        $this->response->body(
            json_encode(
                array(
                    "result" => "success",
                    "update" => "new_comments",
                    "html" => $html,
                    "submittedStamp" => $timestamp,
                    "timestamp" => $latest_timestamp
                )
            )
        );
        return $this->response;
    }
}
