<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use App\Model\Entity\User;
use App\Model\Entity\ForgotPassword;
use App\Model\Entity\Activation;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\View\View;

/**
 * Microblog Controller
 *
 * This controller handles unauthenticated users allowing them to register, sign in
 * activate accounts and even change passwords.
 *
 * @link https://book.cakephp.org/3/en/controllers/pages-controller.html
 * @package Microblog.Controller
 */
class MicroblogController extends AppController
{
    private $Session;
    private $userTable;
    /**
     * Initialize function.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Rangen');
        $this->loadComponent('Email');
        $this->loadComponent('Activation');
        $this->loadComponent('Posts');
        $this->loadComponent('Search');
    }
    /**
     * Executes before any controller action.
     *
     * @param $event I don't know for now.
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        date_default_timezone_set('Asia/Manila');
        $this->Auth->allow(
            [
                'index',
                'login',
                'registration',
                'activation',
                'logout',
                'forgotpassword',
                'resetpassword',
                'lang'
            ]
        );

        $this->Session = $this->getRequest()->getSession();
        $this->userTable = TableRegistry::getTableLocator()->get('Users');
    }
    /**
     * The landing controller action. This is considered as the home page
     * controller action.
     *
     * If the action receives an AJAX request, it will respond with an
     * array of posts.
     *
     * @return void|array An array of posts.
     */
    public function index()
    {
        if (!$this->request->is('ajax')) {
            return;
        }

        $this->autoRender = false;
        if (!$this->Auth->user('id')) {
            $this->response->body(
                json_encode(
                    array(
                        "result" => "failed",
                        "description" =>
                        "You must be logged in to retrieve posts."
                    )
                )
            );
            return $this->response;
        }

        if (empty($this->request->getData())) {
            return;
        }

        if (isset($this->request->data['page'])) {
            $this->response->body(
                $this->Posts->getHomePosts(
                    $this->Auth->user('id'),
                    $this->request->data['page']
                )
            );
            return $this->response;
        }

        if ($this->request->getData('ts') !== null) {
            $this->response->body(
                $this->Posts->getLatestHomePosts(
                    $this->Auth->user('id'),
                    $this->request->getData('ts')
                )
            );
            return $this->response;
        }
    }
    /**
     * Logs out the user.
     *
     * @return void
     */
    public function logout()
    {

        return $this->redirect($this->Auth->logout());
    }
    /**
     * Renders the registration form for new users that would like to
     * create an account.
     *
     * @return void
     */
    public function login()
    {
        if ($this->Auth->user('id')) {
            $this->redirect(
                array(
                    "controller" => "/",
                    "action"     => "index"
                )
            );
        }
        if ($this->request->is('post')) {
            $email_address = $this->request->getData('email_address');
            $password      = $this->request->getData('password');

            if ($email_address === null || $password === null) {
                $this->Flash->error(__('Please enter your email address and password.'));
                return;
            }

            $users = TableRegistry::getTableLocator()->get('Users');
            $query = $users->find('all')
                ->where(
                    [
                        'Users.email_address' => $email_address
                    ]
                );
            $result = $query->first();

            if (empty($result)) {
                $this->Flash->error(
                    __('An account with that email address was not found.')
                );
                return;
            }

            if (!password_verify($password, $result['password'])) {
                $this->Flash->error(
                    __('The email address or password that you entered is incorrect.')
                );
                return;
            }
            if ($result['status'] == 0) {
                $this->Session->write('Activation.email', $email_address);
                $this->redirect(
                    ['controller' => 'Microblog', 'action' => 'activation']
                );
                return;
            }
            $this->Session->write('Search.target', 'users');
            unset($result['password']);
            $this->Auth->setUser($result);
            $this->redirect(
                ['controller' => '/', 'action' => 'index']
            );
        }
    }

    /**
     * Renders the login form for user authentication.
     *
     * @return void
     */
    public function registration()
    {
        if ($this->Auth->user('id')) {
            $this->redirect(
                array(
                    "controller" => "/",
                    "action"     => "index"
                )
            );
        }
        if ($this->request->is('post')) {
            $validator = new \App\Model\Validation\UserValidator();
            $errors = $validator->errors($this->request->getData());
            if ($errors) {
                $this->Flash->warning(
                    __('Please check the form for errors.')
                );
                $this->set('errors', $errors);
                $this->set('form', $this->request->getData());
                return;
            }
            $password = $this->request->getData('password');
            $email_address = $this->request->getData('email_address');
            if ($password != $this->request->getData('repeatPassword')) {
                $this->Flash->warning(
                    __(
                        'The password and the repeat password must match. 
                    Please try again.'
                    )
                );
                $this->set('form', $this->request->getData());
                return;
            }
            $users = TableRegistry::getTableLocator()->get('Users');
            if (!$users->isEmailAvailable($email_address)) {
                $this->Flash->error(
                    __(
                        'The email address '
                        . $email_address .
                        ' has already been used. Please use another email address.'
                    )
                );
                $this->set('form', $this->request->getData());
                return;
            }
            $user = $users->create($this->request->getData());
            if (!$user) {
                $this->Flash->error(
                    __(
                        'There has been an error while sending the activation email.
                    Please try again'
                    )
                );
                $this->set('form', $this->request->getData());
                return;
            }
            if (!$this->Email->sendActivation($user)) {
                $this->Flash->error(
                    __(
                        'There has been an error while sending the activation email.
                    Please try again'
                    )
                );
                $this->set('form', $this->request->getData());
                return;
            }
            $this->Session->write(
                'Activation.email',
                $this->request->getData('email_address')
            );
            $this->setAction('activation');
        }
    }
    /**
     * Renders the activation notice to user. Requires
     * Activation.email to render otherwise will redirect to home.
     * @return void
     */
    public function activation($id = null, $activation_token = null)
    {
        if ($id === null && $activation_token === null) {
            if (!$this->Session->check('Activation.email')) {
                $this->Flash->error('You must first log in your account.');
                $this->redirect(
                    ['controller' => '/', 'action' => 'login']
                );
                return;
            }
            return;
        }
        if (!$this->Activation->check($id, $activation_token)) {
            $this->Flash->error(
                __('The system could not find the account that you wish to activate.')
            );
            $this->redirect(
                ['controller' => '/','action' => 'login']
            );
            return;
        }

        $activation = $this->Activation->activate($id, $activation_token);
        if ($activation == 0) {
            $this->Flash->error(
                __(
                    'There was an error while trying to activate the account. 
                    Please try again later.'
                )
            );
            $this->redirect(
                ['controller' => '/','action' => 'login']
            );
            return;
        }
        if ($activation == 2) {
            $this->Flash->error(
                __(
                    'The account is already activated. You can use it by logging in.'
                )
            );
            $this->redirect(
                ['controller' => '/','action' => 'login']
            );
            return;
        }
        $this->Flash->success(
            __('The account has been successfully activated. You can now log in.')
        );
        $this->redirect(
            ['controller' => '/','action' => 'login']
        );
        return;
    }
    /**
     * Renders the search page to user.
     * @return void
     */
    public function search()
    {
        if ($this->request->is('get')) {
            $searchQuery = isset($this->request->query['q'])
            ? $this->request->query['q'] : '';

            $target = isset($this->request->query['target'])
            ? $this->request->query['target'] : 'users';

            $page = isset($this->request->query['page'])
            ? $this->request->query['page'] : 1;

            if ($searchQuery == "") {
                $this->set('result', array());
                return;
            }

            $searchQuery = filter_var($searchQuery, FILTER_SANITIZE_STRING);
            $this->Session->write('Search.q', $searchQuery);

            $this->set(
                'result',
                $this->Search->execute($searchQuery, $page, $target)
            );
            return;
        }
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $Views = new View($this->request);
            if (empty($this->request->getData())) {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "failed",
                            "description" =>
                            "No search request received."
                        )
                    )
                );
                return $this->response;
            }

            $searchQuery = $this->request->getData('searchQuery');
            $target = $this->request->getData('searchTarget');
            $page = $this->request->getData('searchPage');
            $this->Session->write('Search.target', $target);

            if ($searchQuery == "") {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "success",
                            "html"   => $Views->element('Search/search_guide'),
                            "overallCount" => 0,
                            "pages" => 0,
                            "currentPage" => 1,
                            "currentOffset" => 0
                        )
                    )
                );
                return $this->response;
            }

            $result = $this->Search->execute($searchQuery, $page, $target);

            if ($result['overallCount'] == 0) {
                $this->response->body(
                    json_encode(
                        array(
                            "result" => "success",
                            "html"   => $Views->element('Search/search_empty'),
                            "overallCount" => 0,
                            "pages" => 0,
                            "currentPage" => 1,
                            "currentOffset" => 0
                        )
                    )
                );
                return $this->response;
            }

            $html = "";

            if ($target == "users") {
                foreach ($result['returnRows'] as $user) {
                    $html = $html . $Views->element(
                        'Search/user_result',
                        array(
                            'user' => $user
                        )
                    );
                }
            } else {
                foreach ($result['returnRows'] as $post) {
                    $html = $html . $Views->element(
                        'Search/post_result',
                        array(
                            'post' => $post
                        )
                    );
                }
            }
            $this->response->body(
                json_encode(
                    array(
                        "result" => "success",
                        "html"   => $html,
                        "overallCount" => $result['overallCount'],
                        "pages" => $result['pages'],
                        "currentPage" => $page
                    )
                )
            );
            return $this->response;
        }
    }
    /**
     * The forgot password action that processess the forgot
     * password operations.
     *
     * @return void
     */
    public function forgotPassword()
    {
        if ($this->Auth->user('id')) {
            $this->Flash->warning(
                MUST_BE_LOGGED_OUT
            );
        }
        if ($this->request->is('post')) {
            $result = $this->Email->sendForgotPassword(
                $this->request->getData("inputEmailAddress")
            );

            if ($result == 3) {
                $this->Flash->warning(
                    __(
                        'The email address was not found.
                        Please check the entered email address.'
                    )
                );
                return;
            }
            if ($result == 2) {
                $this->Flash->warning(
                    __(
                        'Please wait for a while to send another 
                        forgot password request for this account.'
                    )
                );
                return;
            }
            if ($result == 0) {
                $this->Flash->error(
                    __(
                        'There has been an error while trying to reset your password. 
                        Please try again in a few minutes.'
                    )
                );
                return;
            }
            $this->Flash->success(
                'We have sent an email to your email address. 
                Kindly check your inbox and click the reset password link to reset your password.'
            );
        }
    }
    /**
     * Resets the password of an account when the id and reset token
     * matched.
     *
     * @param string $id The id of the reset password record in the database.
     * @param string $reset_token The matching token of the id.
     */
    public function resetPassword($id = null, $reset_token = null)
    {
        if ($this->request->is('post')) {
            $newPassword = $this->request->getData('inputPassword');
            if ($newPassword == "") {
                $this->Flash->error(
                    'Please enter a valid password.'
                );
                return;
            }
            if (strlen($newPassword) < 8) {
                $this->Flash->warning(
                    'Please enter a password that has 8 characters or more for increased security.'
                );
                return;
            }
            $users = TableRegistry::getTableLocator()->get('Users');
            $user = $users->get($this->Session->read('ForgotPassword.user_id'));
            if (empty($user)) {
                $this->Flash->error(
                    'The account that you are trying to reset the password does not exist. Please try again.'
                );
                return;
            }
            $user->password = password_hash($newPassword, PASSWORD_DEFAULT);
            if (!$users->save($user)) {
                $this->Flash->error(
                    'An error has occurred while trying to reset your password. Please try again.'
                );
                return;
            }
            $this->Flash->success(
                'You have successfully changed your password. You may now log in your account'
            );
            $forgotPasswords = TableRegistry::getTableLocator()->get('ForgotPasswords');
            $forgotPasswords->delete($forgotPasswords->get($id));
            $this->Session->consume('ForgotPassword.user_id');
            $this->redirect(array('controller' => '/','action' => 'login'));
            return;
        }
        if ($id == null || $reset_token == null) {
            $this->Flash->error(
                'The credentials that you passed is invalid. Please try again'
            );
            $this->redirect(
                [
                    'controller' => '/','action' => 'forgotpassword'
                ]
            );
            return;
        }
        $forgotPasswords = TableRegistry::getTableLocator()->get('ForgotPasswords');
        $forgotPassword = $forgotPasswords->find()->where(
            [
                'id' => $id,
                'reset_token' => $reset_token
            ]
        )->first();

        if (empty($forgotPassword)) {
            $this->Flash->error(
                'An invalid request has been received. 
                If you clicked a link from your email to reset your password, 
                kindly request for another by entering your email address below.'
            );
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        if ($forgotPassword->reset_token != $reset_token) {
            $this->Flash->error(
                'An invalid request has been received. 
                If you clicked a link from your email to reset your password, 
                kindly request for another by entering your email address below.'
            );
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        $requestDate = strtotime($forgotPassword->created);
        $dateNow     = strtotime(date('Y-m-d H:i:s'));
        $minutes = round(($dateNow - $requestDate) / 60, 2);
        if ($minutes > 60) {
            $this->Flash->error(
                'This password reset request has expired. 
                Kindly request for another by entering your email address below.'
            );
            $forgotPasswords->delete($forgotPassword);
            $this->redirect(array('controller' => '/','action' => 'forgotpassword'));
            return;
        }
        $this->Session->write('ForgotPassword.user_id', $forgotPassword->user_id);
    }
    /**
     * An action that changes the language
     * of the application upon request.
     *
     * @return void
     */
    public function lang()
    {
        $this->autoRender = false;
        $lang = $this->request->getData('lang-selector');
        $this->Lang->changeLanguage($lang);
        return $this->redirect($this->referer());
    }
}
