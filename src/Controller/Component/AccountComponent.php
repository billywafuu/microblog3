<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;

/**
 * Provides assistive functions on user account
 * related operations.
 */
class AccountComponent extends Component
{
    /**
    * Loads up any other components that will be used.
    *
    * @var array
    */
    public $components = ['Email', 'Upload'];

    /**
     * The users table.
     *
     * @var Cake\ORM\TableRegistry
     */
    private $usersTable;

    /**
     * Initialization level. Loads the users table
     * into the private usersTable variable.
     *
     * @param array $config
     * @return void
     */
    public function initialize(array $config)
    {
        $this->usersTable = TableRegistry::getTableLocator()->get('users');
    }

    /**
     * Checks a user account if it exists.
     *
     * It will return true only if the account
     * has a status = 1.
     *
     * @param int $id The user id of the account to check.
     * @return bool Returns true if it exists, false when not.
     */
    public function exists($id)
    {
        if ($this->usersTable->get($id)) {
            return true;
        }
        return false;
    }
    /**
     * Changes the password of a user account provided the
     * current password is correct.
     *
     * @param string $id The user id of the account.
     * @param string $currentPassword The current password of the account.
     * @param string $newPassword The new password to assign.
     * @return int 0 - Failed, 1 - Success, 2 - Wrong Current Password
     */
    public function changePassword($id, $currentPassword, $newPassword)
    {
        try {
            $user = $this->usersTable->get($id);
            if (!password_verify($currentPassword, $user->password)) {
                return 2;
            }
            if (strlen($newPassword) < 8) {
                return 0;
            }
            $user->password = password_hash($newPassword, PASSWORD_BCRYPT);
            $this->usersTable->save($user);
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }
    /**
     * Updates the user account.
     *
     * This function directly updates user account.
     * It takes an id and array of value. Does not
     * update email address as it requires separate
     * checking. Use updateEmailAddress() for that.
     *
     * @param int $id The id of the user to update.
     * @param array $values The array of values.
     * @return int 0 - Failed, 1 - Success, 2 - Date Error
     */
    public function update($id, array $values)
    {
        try {
            $user = $this->usersTable->get($id);
            if (empty($user)) {
                return 0;
            }

            if (!(bool)strtotime($values['birthdate'])) {
                return 2;
            }

            $user->first_name = $values['first_name'];
            $user->middle_name = $values['middle_name'];
            $user->last_name = $values['last_name'];
            $user->birthdate = new FrozenTime($values['birthdate']);

            if (!$this->usersTable->save($user)) {
                return 0;
            }
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }

    /**
     * Updates the email address of the user.
     *
     * It also checks if the email address is available
     * when changing to another email address.
     *
     * @param int $id The id of the user that will update the email.
     * @param string $email_address The email address to insert.
     * @return int 0 - Failed, 1 - Success, 2 - Email Unavailable
     */
    public function updateEmailAddress($id, $email_address)
    {
        try {
            $user = $this->usersTable->get($id);
            if (empty($user)) {
                return 0;
            }
            if (!$this->usersTable->isEmailAvailable($email_address)) {
                return 2;
            }
            $user->email_address = $email_address;
            $user->status = 0;
            if (!$this->usersTable->save($user)) {
                return 0;
            }
            if (!$this->Email->sendActivation($user, 'Re-activation')) {
                return 0;
            }
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }

    /**
     * Uploads new or replaces user account image.
     *
     * @param int $id The user id to upload the image for.
     * @param file $file The PHP file.
     * @return array The return array contains the result and description.
     */
    public function uploadImage($id, $file)
    {
        try {
            $user = $this->usersTable->get($id);
            if (!$user) {
                return [
                    'result' => 'failed',
                    'description' => 'No account found with that id.'
                ];
            }
            $result = $this->Upload->file($file, 'user', $id);
            if ($result == 0) {
                return [
                    'result' => 'failed',
                    'description' => 'Failed to upload file.'
                ];
            }
            if ($result == 2) {
                return [
                    'result' => 'invalid_file_type',
                    'description' => 'Failed to upload file.'
                ];
            }
            $currentFile = $user->id . '.' . $user->image_file_type;
            $imageFileType = $this->Upload->getFileType($file);
            if (file_exists($currentFile)) {
                unlink($currentFile);
            }

            $user->image_file_type = $imageFileType;
            $this->usersTable->save($user);

            return [
                'result' => 'upload_success',
                'description' => 'The upload was successful.'
            ];
        } catch (\Exception $e) {
            debug($e);
            return;
        }
    }
    /**
     * Crops the existing uploaded photo of the
     * user account.
     *
     * @param int $id The user id.
     * @param base64 $base64 The base64 string.
     * @return array The return array contains the result and description.
     */
    public function cropExisting($id, $base64)
    {
        $user = $this->usersTable->get($id);
        if ($user->image_file_type == "") {
            return [
                'result' => 'failed',
                'description' => 'The upload failed.'
            ];
        }
        if (!base64_decode($base64)) {
            return [
                'result' => 'failed',
                'description' => 'The upload failed.'
            ];
        }
        $filename = $user->id . '.' . $user->image_file_type;
        $result = $this->Upload->uploadBase64($base64, 'user', $filename);
        if ($result == 1) {
            return [
                'result' => 'upload_success',
                'description' => 'The upload was successful.'
            ];
        } else {
            return [
                'result' => 'failed',
                'description' => 'The upload failed.'
            ];
        }
    }
}
