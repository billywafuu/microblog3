<?php

namespace App\Controller\Component\API;

use Cake\Controller\Component;
use App\Model\Entity\Client;

/**
 * Authentication
 * 
 * The authentication component handles the logic needed when
 * authenticating clients with API.
 */
class AuthenticationComponent extends Component
{
    /**
     * Loads up request+`
     *
     * @var array
     */
    public $components = ['Rangen'];

    /**
     * Encodes the user id and the access token into
     * base64 with <user_id>:<access_token> and return it
     * as the Authorization token,
     *
     * @param int $user_id
     * @param string $access_token
     * @return base64 The token
     */
    public function createAuthorization($user_id, $access_token)
    {
        // Compile the user_id, key, and client secret into a string.
        $authorization = $user_id . ':' . $access_token;

        // Encode to base64.
        $authorization = base64_encode($authorization);

        // Return the authorization token.
        return $authorization;
    }

    /**
     * Creates a new client for the user with a token.
     *
     * @param int $user_id
     * @param string $token
     * @param Cake\Http\Request $request
     * @return App\Model\Entity\Client Returns a new client object.
     */
    public function createClient($user_id, $token, $request)
    {
        // Create new client.
        $client = new Client();

        // Assign new values to the client. 
        $client->user_id = $user_id;

        // Assign the key.
        $client->access_token = $token;

        // Set an expiry.
        $client->date_expiry =  date(
            'Y-m-d H:i:s',
            strtotime(date('Y-m-d H:i:s') . " +2 days")
        );

        // Set the user agent.
        $client->user_agent = $request->getHeaderLine('User-Agent');

        // Set the source IP address.
        $client->ip_address = $request->clientIp();

        // Return.
        return $client;
    }
}
