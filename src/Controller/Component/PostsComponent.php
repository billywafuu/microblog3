<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\View\View;

/**
 * A component that manages all posts retrieval and
 * pagination.
 *
 * This component returns an array of paging information and
 * the posts.
 */
class PostsComponent extends Component
{
    /**
     * The Posts table.
     *
     * @var Cake\ORM\TableRegistry
     */
    private $Posts;

    /**
     * Loads up any other components that will be used.
     *
     * @var array
     */
    public $components = ['Paging'];

    public function initialize(array $config)
    {
        $this->Posts = TableRegistry::getTableLocator()->get('Posts');
    }

    /**
     * Gets all the posts of a user profile. This function returns
     * an array of post entity and paging information.
     *
     * @param int $id The id of the user.
     * @param int|null $page The page number of posts.
     * @param int|null $limit The limit of posts per page.
     * @return array|null Returns an array of Post entity and paging information.
     * Returns null if no post was found. Only returns array of Post entity if
     * $page and $limit is null.
     */
    public function getProfilePosts($id, $page = null, $limit = null)
    {
        $posts = $this->Posts->find()
            ->where(
                [
                    'Posts.deleted' => 0,
                    'Posts.user_id' => $id
                ]
            )
            ->order(
                ['Posts.created' => 'DESC']
            )
            ->contain(
                ['Retweet', 'Owner', 'Likes']
            )->toArray();

        $overallPostCount = count($posts);
        if ($overallPostCount == 0) {
            return null;
        }

        if ($page === null && $limit === null) {
            return $posts;
        }

        $maxNumberOfPages = ceil($overallPostCount / $limit);

        if ($page > $maxNumberOfPages) {
            $page = $maxNumberOfPages;
        }

        if ($page <= 0) {
            $page = 1;
        }

        $offset = (($page - 1) * $limit);
        $posts = array_slice($posts, $offset, $limit);

        $prevPage = $page - 1;
        $nextPage = $page + 1;

        if ($prevPage == 0) {
            $prevPage = 1;
        }
        if ($nextPage > $maxNumberOfPages) {
            $nextPage = $maxNumberOfPages;
        }

        return array(
            'posts' => $posts,
            'pagination' => [
                "numberOfPages" => $maxNumberOfPages,
                "currentPage" => $page,
                "prevPage" => $prevPage,
                "nextPage" => $nextPage
            ]
        );
    }
    /**
     * Gets the posts of all the users that the current
     * user follows.
     *
     * A home posts is a collection of posts that the other
     * user posted. The post of other user will be included in
     * a particular home post if the user is being followed.
     *
     * @param int $id The user id.
     * @param int $page The page number of the posts.
     * @return array An array of posts.
     */
    public function getHomePosts($id, $page)
    {
        $html = null;
        $Views = new View($this->request);
        $currentPage = $page;
        $followsTable = TableRegistry::getTableLocator()->get('Follows');
        $overallFollowings = $followsTable->find()
        ->where(
            [
                'user_id' => $id
            ]
        )->contain(
            [
                'Following', 'Posts'
            ]
        )->contain(
            [
                'Posts.Retweet',
                'Posts.Owner',
                'Posts.Likes'
            ]
        )->order(
            [
                'created' => 'ASC'
            ]
        )->toArray();
        $posts = array();
        foreach ($overallFollowings as $user) {
            foreach ($user->posts as $post) {
                array_push($posts, $post);
            }
        }
        usort(
            $posts,
            function ($a, $b) {
                return strtotime($b->created) - strtotime($a->created);
            }
        );
        if (count($posts) == 0) {
            return json_encode(
                array(
                    "result" => "no_posts",
                    "html" => $Views->element('Posts/hp_no_posts')
                )
            );
        }

        $paged = $this->Paging->execute($posts, $currentPage, 10);
        $lockTimestamp = 0;

        foreach ($paged['result'] as $post) {
            if ($lockTimestamp == 0) {
                $postsTable = TableRegistry::getTableLocator()->get('Posts');
                $record = $postsTable->find()
                    ->select(
                        [
                            'ts' => 'UNIX_TIMESTAMP(created)'
                        ]
                    )
                    ->where(
                        [
                            'id' => $post->id,
                        ]
                    )->first();
                $lockTimestamp = $record->ts;
            }
            $html = $html . $Views->element(
                'Posts/post',
                array(
                    'post' => $post,
                    'owner' => $post->owner,
                    'retweet' => $post->retweet,
                    'likes' => $post->likes
                )
            );
        }
        $maxNumberOfPages = $paged['pagination']['maxNumberOfPages'];
        $endOfFeeds = ($maxNumberOfPages == $currentPage) ? true : false;
        return json_encode(
            array(
                "result" => "new_posts",
                "html" => $html,
                "currentPage" => $currentPage,
                "endOfFeeds" => $endOfFeeds,
                "timestamp" => $lockTimestamp
            )
        );
    }

    /**
     * Gets the latest home posts.
     *
     * @param int $id The user id.
     * @param int $timestamp The UNIX timestamp reference.
     * It will retrieve posts that has a timestamp higher than
     * the value of this given timestamp.
     *
     * @return array An array of posts.
     */
    public function getLatestHomePosts($id, $timestamp)
    {
        $html = null;
        $Views = new View($this->request);
        $followsTable = TableRegistry::getTableLocator()->get('Follows');
        $tsFilter = ['UNIX_TIMESTAMP(Posts.created) >' . $timestamp];
        $overallFollowings = $followsTable->find()
        ->where(
            [
                'user_id' => $id
            ]
        )->contain(
            [
                'Following', 'Posts' => function ($q) use ($tsFilter) {
                    return $q->where(
                        $tsFilter
                    );
                }
            ]
        )->contain(
            [
                'Posts.Retweet',
                'Posts.Owner',
                'Posts.Likes'
            ]
        )->order(
            [
                'created' => 'ASC'
            ]
        )->toArray();
        $posts = array();
        foreach ($overallFollowings as $user) {
            foreach ($user->posts as $post) {
                array_push($posts, $post);
            }
        }
        if (count($posts) == 0) {
            return
                json_encode(
                    array(
                        "result" => "no_posts",
                        "description" => "No updated posts.",
                        "date_request"
                        => gmdate("Y-m-d H:i:s", $this->request->data['ts'])
                    )
                );
        }
        usort(
            $posts,
            function ($a, $b) {
                return strtotime($b->created) - strtotime($a->created);
            }
        );
        if (count($posts) == 0) {
            return json_encode(
                array(
                    "result" => "no_posts",
                    "html" => $Views->element('Posts/hp_no_posts')
                )
            );
        }

        $paged = $this->Paging->execute($posts, 0, 10);
        $lockTimestamp = 0;
        foreach ($paged['result'] as $post) {
            if ($lockTimestamp == 0) {
                $postsTable = TableRegistry::getTableLocator()->get('Posts');
                $record = $postsTable->find()
                    ->select(
                        [
                            'ts' => 'UNIX_TIMESTAMP(created)'
                        ]
                    )
                    ->where(
                        [
                            'id' => $post->id,
                        ]
                    )->first();
                $lockTimestamp = $record->ts;
            }
            $html = $html . $Views->element(
                'Posts/post',
                array(
                    'post' => $post,
                    'owner' => $post->owner,
                    'retweet' => $post->retweet,
                    'likes' => $post->likes
                )
            );
        }
        return json_encode(
            array(
                "result" => "new_posts",
                "html" => $html,
                "timestamp" => $lockTimestamp
            )
        );
    }
}
