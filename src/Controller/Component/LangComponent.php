<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\I18n;

/**
 * A component that houses the function that is
 * basically used to change language.
 */
class LangComponent extends Component
{
    public function changeLanguage($locale)
    {
        i18n::setLocale($locale);
        $controller = $this->_registry->getController();
        $session = $controller->request->session();
        $session->write('locale', $locale);
    }
}
