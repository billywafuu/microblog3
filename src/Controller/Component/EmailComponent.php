<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Activation;
use App\Model\Entity\User;
use App\Model\Entity\ForgotPassword;

/**
 * Email Component
 *
 * A component that houses the email sending logics
 * of Microblog.
 */
class EmailComponent extends Component
{
    /**
     * Loads up any other components that will be used.
     *
     * @var array
     */
    public $components = ['Rangen'];

    /**
     * Sends an activation email to a specific email address.
     * When passing an Entity, it will automatically reads the
     * email address of the User entity and its first name to
     * address.
     *
     * @param string $user The user entity.
     * @param string $id The activation id.
     * @param string $access_token The access token for the activation.
     * @return bool Returns true on success or false.
     */
    public function sendActivation(
        $user,
        $title = 'Registration'
    ) {
        try {
            $activation_token = $this->Rangen->generate();
            $code = $this->Rangen->generate(6);
            $activations = TableRegistry::getTableLocator()->get('Activations');
            $activation  = new Activation(
                [
                    'activation_token' => $activation_token,
                    'user_id' => $user->id,
                    'code' => $code
                ]
            );
            if (!$activations->save($activation)) {
                return false;
            }

            $email = new Email();
            $email->transport('default');
            $email
                ->template('activation')
                ->emailFormat('html')
                ->subject('Microblog Registration')
                ->to($user->email_address)
                ->from(
                    [
                        'microblog3@iloiloclassified.com' => $title
                    ]
                )
                ->setViewVars(
                    [
                        'id' => $activation->id,
                        'activation_token' => $activation_token,
                        'code' => $code,
                        'first_name' => $user->get('first_name')
                    ]
                )
                ->send();
            return true;
        } catch (\Exception $e) {
            debug($e);
            return false;
        }
    }
    /**
     * Sends a password reset link to the specified email address.
     *
     * @param string $email_address
     * @return int 0 - Failed, 1 - Success, 2 - Delay, 3 - User not found.
     */
    public function sendForgotPassword($email_address)
    {
        try {
            $users = TableRegistry::getTableLocator()->get('Users');
            $user = $users->find(
                'byEmailAddress',
                ['email_address' => $email_address]
            );
            if (empty($user)) {
                return 3;
            }
            $forgotPasswords = TableRegistry::getTableLocator()->get('ForgotPasswords');
            $last_request = $forgotPasswords->find(
                'byLastRequest',
                ['user_id' => $user->id]
            );

            if (!empty($last_request)) {
                $dateNow     = strtotime(date('Y-m-d H:i:s'));
                $dateRequest = strtotime($last_request->created);
                $minutes = round(($dateNow - $dateRequest) / 60, 2);
                if ($minutes < 5) {
                    return 2;
                }
                $forgotPasswords->delete($last_request);
            }
            $reset_token = $this->Rangen->generate();
            $newForgotPassword = new ForgotPassword();
            $newForgotPassword->reset_token = $reset_token;
            $newForgotPassword->user_id = $user->id;
            $newForgotPassword->created = date('Y-m-d H:i:s');
            $forgotPasswords->save($newForgotPassword);

            $email = new Email();
            $email->transport('default');

            $email
                ->template('forgotpassword')
                ->emailFormat('html')
                ->subject('Microblog Forgot Password')
                ->to($user->email_address)
                ->from(
                    [
                        'microblog3@iloiloclassified.com' => 'Forgot Password'
                    ]
                )
                ->setViewVars(
                    [
                        'id' => $newForgotPassword->id,
                        'reset_token' => $reset_token,
                        'first_name' => $user->first_name,
                        'user_id' => $user->id
                    ]
                )
                ->send();
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }
}
