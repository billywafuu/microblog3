<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\View\View;

/**
 * A component that manages the resource searching.
 */
class SearchComponent extends Component
{
    /**
     * Loads up any other components that will be used.
     *
     * @var array
     */
    public $components = ['Paging'];

    /**
     * Executes a search pattern.
     *
     * Executes a search pattern using the given
     * target and the page to return. The action will
     * return the last page if the page is higher than
     * the maximum page of result.
     *
     * @param string $searchQuery The string to match the search.
     * @param string $target The target. Can be users or posts. Defaults to users.
     * @param string $page The page of the results to return.
     * @return array An array of result.
     */
    public function execute($searchQuery, $page, $target = 'users')
    {
        try {
            $result = array();
            if ($target == "users" || $target == "User") {
                $users = TableRegistry::getTableLocator()->get('Users');
                $result = $users->find()
                    ->where(
                        [
                            "OR" => [
                                "CONCAT(last_name, ' ', first_name) 
                                LIKE '%$searchQuery%'",
                                "CONCAT(first_name, ' ', last_name) 
                                LIKE '%$searchQuery%'",
                                "CONCAT(first_name, ' ', middle_name, ' ', last_name) 
                                LIKE '%$searchQuery%'",
                                "email_address LIKE '%$searchQuery%'"
                            ],
                            "status = 1",
                            "deleted = 0"
                        ]
                    )
                    ->toArray();
            }
            if ($target == "posts" || $target == "Post") {
                $posts = TableRegistry::getTableLocator()->get('Posts');
                $result = $posts->find()
                    ->where(
                        [
                            "OR" => [
                                "title LIKE '%$searchQuery%'",
                                "body LIKE '%$searchQuery%'",
                            ],
                            "deleted = 0"
                        ]
                    )
                    ->toArray();
            }
            if ($result == null) {
                return array(
                'returnRows' => array(),
                'model' => $target,
                'overallCount' => 0,
                'pages' => 0
                );
            }
            $result = $this->Paging->execute($result, $page, 10);
            return array(
                'returnRows' => $result['result'],
                'model' => $target,
                'overallCount' => $result['pagination']['overallCount'],
                'pages' => $result['pagination']['maxNumberOfPages']
            );
        } catch (\Exception $e) {
            debug($e);
            return null;
        }
    }
}
