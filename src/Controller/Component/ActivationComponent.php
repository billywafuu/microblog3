<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Follow;

/**
 * A component that manages user account
 * activation.
 */
class ActivationComponent extends Component
{
    /**
     * The activations table.
     *
     * @var TableRegistry
     */
    private $table;

    /**
     * The users table.
     *
     * @var TableRegistry
     */
    private $users;

    /**
     * Initialization function to initialize the activation component.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table = TableRegistry::getTableLocator()->get('activations');
        $this->users = TableRegistry::getTableLocator()->get('users');
    }

    /**
     * Checks id and activation token if such
     * combination exists.
     *
     * @param string $id
     * @param string $activation_token
     * @return bool Returns true if found, False if not.
     */
    public function check($id, $activation_token)
    {
        $activation = $this->table->find()->where(
            [
                'id' => $id,
                'activation_token' => $activation_token
            ]
        )->first();

        if (empty($activation)) {
            return false;
        }

        return true;
    }

    /**
     * Activates user account that currently uses the
     * id and activation_token.
     *
     * @param string $id
     * @param string $activation_token
     * @return int 0 - Failed, 1 - Success, 2 - Already activated.
     */
    public function activate($id, $activation_token)
    {
        $activation = $this->table->find()->where(
            [
                'id' => $id,
                'activation_token' => $activation_token
            ]
        )->first();
        $user = $this->users->get($activation->user_id);

        if (empty($user)) {
            return 0;
        }

        if ($user->status == 1) {
            return 2;
        }

        $user->status = 1;
        if (!$this->users->save($user)) {
            return 0;
        }

        $this->table->delete($activation);
        $this->users->deleteAll(
            [
                'email_address' => $user->email_address,
                'status' => 0
            ]
        );
        $user->setFollow($user->id);
        return 1;
    }
}
