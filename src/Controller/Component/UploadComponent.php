<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * Provides a helper function that is considered
 * as the base upload function of any function
 * that has file uploading. This function is
 * used by profile photo uploading and post photo
 * uploading.
 *
 */
class UploadComponent extends Component
{
    /**
     * Uploads an image file.
     *
     * Uploads a file to the webroot by
     * indicating a destination directory.
     *
     * Only .jpg, .png and .gif files are allowed
     * by the function.
     *
     * @param File $file The PHP $_FILE
     * @param string $destination The destination where to place
     * the file. Creates the directory if it does not exist.
     * @param string $filename The name of the file on save.
     * @return int 0 - Failed, 1 - Success, 2 - Invalid file type
     */
    public function file($file, $destination, $filename)
    {
        try {
            if ($file === null) {
                return 0;
            }

            if (empty($file['name'])) {
                return 0;
            }

            if (!getimagesize($file["tmp_name"])) {
                return 2;
            }

            if (!file_exists($destination . '/')) {
                mkdir($destination . '/', 0777, true);
            }

            $uploadPath = $destination . '/';
            $imageFileType = strtolower(
                pathinfo(
                    $uploadPath . basename($file["name"]),
                    PATHINFO_EXTENSION
                )
            );

            $filename = $filename . '.' . $imageFileType;
            if ($imageFileType != "jpg"
                && $imageFileType != "png"
                && $imageFileType != "jpeg"
                && $imageFileType != "gif"
            ) {
                return 2;
            }

            if ($file["size"] >= 50000000) {
                return 0;
            }

            if (file_exists($filename)) {
                unlink($filename);
            }

            if (!move_uploaded_file($file['tmp_name'], $uploadPath . $filename)) {
                return 0;
            }

            if ($destination == "user") {
                $source = $uploadPath . $filename;
                $copy   = explode('.', $filename);
                $copy   = $uploadPath . $copy[0] . '_original.' . $copy[1];

                if (file_exists($copy)) {
                    unlink($copy);
                }

                if (!copy($source, $copy)) {
                    return 0;
                }
            }
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }
    /**
     * Gets the file type of a file.
     *
     * @param $_FILE $file The file to read.
     * @return string Returns the file type without the dot (.)
     */
    public function getFileType($file)
    {
        return strtolower(
            pathinfo(
                basename($file["name"]),
                PATHINFO_EXTENSION
            )
        );
    }

    /**
     * Uploads and converts base64 string into
     * an image file.
     *
     * @param base64 $base64 The file to upload.
     * @param string $destination The destination.
     * @param string $filename The name of the file upon upload.
     * @return int 0 - Failed, 1 - Success
     */
    public function uploadBase64($base64, $destination, $filename)
    {
        try {
            $parts = explode(",", $base64);
            $image = base64_decode($parts[1]);
            $file = $destination . '/' . $filename;
            if (!file_exists($destination . '/')) {
                mkdir($destination . '/', 0777, true);
            }

            if (file_exists($file)) {
                unlink($file);
            }

            if (!file_put_contents($file, $image)) {
                return 0;
            }
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }
}
