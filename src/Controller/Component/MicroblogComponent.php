<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * Microblog Component
 *
 * Holds all the reusable functions that is used
 * on Microblog Controller or other controllers.
 */
class MicroblogComponent extends Component
{

}
