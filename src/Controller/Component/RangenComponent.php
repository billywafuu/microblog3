<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * Random Generator Component (Rangen)
 *
 * Functions that return random VARCHARS or INTEGER
 * values with specified length.
 */
class RangenComponent extends Component
{
    /**
     * Generate Function
     *
     * A function that generates a random string of numeric or alphanumeric with
     * a specified length.
     *
     * @param integer $length The length of the string that would be returned. Default 16.
     * @param boolean $intOnly Set to True if you want to return a numeric string. Defaults to False (Alphanumeric)
     * @return string The random string of iether alpha numerics or numerics with specified length.
     */
    public function generate($length = 16, $intOnly = false)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz12345678901234567890';
        if ($intOnly) {
            $chars = '12345678901234567890';
        }
        $output = array();
        $len = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $output[$i] = $chars[rand(0, $len - 1)];
        }
        return strtoupper(implode($output));
    }
}
