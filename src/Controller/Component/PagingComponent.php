<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;

/**
 * Paging Component for custom pagination.
 *
 * Provides assistive functions on bulk data
 * paging operations.
 */
class PagingComponent extends Component
{
    /**
     * Execute paging on array data or entities.
     *
     * Executes paging by slicing the given list of
     * entities or array. Indicate which page you
     * would like to return and the limit per page.
     *
     * @param mixed $data List entities or array data.
     * @param int $page Page number to return.
     * @param int $limit The limit per page.
     * @return array|null Returns a result object and pagination object
     * in an array. (result, pagination)
     *
     * The pagination object contains:
     * currentPage
     * prevPage
     * nextPage
     * overallCount
     * count
     * maxNumberOfPages
     */
    public function execute($data, $page, $limit)
    {
        try {
            if ($data === null) {
                return null;
            }

            $count = count($data);
            if ($count == 0) {
                return null;
            }

            $currentPage = $page;
            $maxNumberOfPages = ceil($count / $limit);

            if ($currentPage > $maxNumberOfPages) {
                $currentPage = $maxNumberOfPages;
            }

            if ($currentPage <= 0) {
                $currentPage = 1;
            }

            $offset = ($currentPage - 1) * $limit;
            $result = array_slice($data, $offset, $limit);
            $prevPage = ($currentPage > 1) ? $currentPage - 1 : 1 ;
            $nextPage = ($currentPage < $maxNumberOfPages) ? $currentPage + 1 : $maxNumberOfPages;

            return array(
                'result' => $result,
                'pagination' => [
                    'currentPage' => $currentPage,
                    'prevPage' => $prevPage,
                    'nextPage' => $nextPage,
                    'overallCount' => $count,
                    'count' => count($result),
                    'maxNumberOfPages' => $maxNumberOfPages
                ]
            );
        } catch (\Exception $e) {
            debug($e);
            return null;
        }
    }
}
