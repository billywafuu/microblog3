<?php namespace App\Controller\Api;

use App\Controller\AppController;
use App\Model\Entity\Post;
use App\Model\Validation\PostValidator;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use App\Libraries\ResponseComponent;

/**
 * Posts Controller controls all requests regarding
 * post.
 */
class PostsController extends AppController
{
    /**
     * List of components to use.
     *
     * @var array
     */
    public $components = ['Upload'];

    /**
     * Initialize.
     *
     * @return void
     */

    public function initialize() {
        parent::initialize();
        $this->loadComponent(
            'Response',
            [
                'className' => '\App\Controller\Component\API\ResponseComponent'
            ]
        );
        $this->Auth->allow(
            [
                'create',
                'retrieve',
                'update',
                'delete',
                'like',
                'retweet'
            ]
        );
        $this->Posts = TableRegistry::getTableLocator()->get('posts');
        $this->Likes = TableRegistry::getTableLocator()->get('likes');
        $this->Clients = TableRegistry::getTableLocator()->get('clients');
    }
    /**
     * Creates a new post. Receives post payload.
     *
     * @return Response
     */
    public function create()
    {
        // Receive the payload.
        $validator = new PostValidator();
        $errors = $validator->errors($this->request->getData());

        // If there are errors. Return success but with form_errors
        // status. Returning the previous payload and the errors.
        if ($errors) {
            return $this->Response->success(
                $this->response,
                'form_errors',
                [
                    'payload' => $this->request->getData(),
                    'errors' => $errors
                ]
            );
        }

        // Create new post.
        $post = new Post();

        // Get user id. 
        $user_id = $this->Clients->getUserId($this->request);

        if ($user_id === null) {
            return $this->Response->unauthorized(
                $this->response
            );
        }

        $post->title   = $this->request->getData('title');
        $post->body    = nl2br($this->request->getData('body'));
        $post->created = date('Y-m-d H:i:s');
        $post->post_id = null;
        $post->user_id = $user_id;

        // Save the new post. 
        if (!$this->Posts->save($post)) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // If there are no following files.
        if (empty($_FILES['postImage']['name'])) {
            if ($post->title == "" && $post->body == "") {
                return $this->Response->failed(
                    $this->response
                );
            }
        }

        // Assign the file type.
        $post->image_file_type = $this->Upload->getFileType($_FILES['postImage']);

        // Upload the file.
        $result = $this->Upload->file(
            $_FILES['postImage'],
            'post',
            $post->id . '.' . $post->image_file_type
        );

        // If the client sent an invalid file type. Return fail.
        if ($result == 2) {
            return $this->Response->failed(
                $this->response
            );
        }

        // Indicates upload error. Internal error.
        if ($result == 0) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // Save the post.
        if (!$this->Posts->save($post)) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // Return the created post id so that client
        // can use it to immediately redirect to the Post.
        return $this->Response->success(
            $this->response,
            'save_success',
            [
                'id' => $post->id
            ]
        );
    }
    /**
     * Updates a post. Receives Post payload.
     *
     * @param int $id The id of the post.
     * @return Response
     */
    public function update($id)
    {
        if ($id === null) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // Get the post.
        $post = $this->Posts->get($id);

        // If the requested post does not exist.
        if (!$post) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // If post has been deleted.
        if ($post->deleted == 1) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        //====================================================
        // TODO: Create an ownership check. TEMPORARY
        //====================================================
        $validator = new PostValidator();
        $errors = $validator->errors($this->request->getData());
        if ($errors) {
            return $this->Response->success(
                $this->response,
                'form_errors',
                [
                    'payload' => $this->request->getData(),
                    'errors' => $errors
                ]
            );
        }

        $post->title = $this->request->getData('title');
        $post->body = $this->request->getData('body');

        if (empty($_FILES['postImage']['name'])) {
            if (!$this->Posts->save($post)) {
                return $this->Response->internalError(
                    $this->response
                );
            }
            return $this->Response->success(
                $this->response,
                'save_success',
                [
                    'id' => $post->id
                ]
            );
        }

        // Assign the file type.
        $post->image_file_type = $this->Upload->getFileType($_FILES['postImage']);

        // Upload the file.
        $result = $this->Upload->file(
            $_FILES['postImage'],
            'post',
            $post->id . '.' . $post->image_file_type
        );

        // If the client sent an invalid file type. Return fail.
        if ($result == 2) {
            return $this->Response->failed(
                $this->response
            );
        }

        // Indicates upload error. Internal error.
        if ($result == 0) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // Save the post.
        if (!$this->Posts->save($post)) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // Return the created post id so that client
        // can use it to immediately redirect to the Post.
        return $this->Response->success(
            $this->response,
            'save_success',
            [
                'id' => $post->id
            ]
        );
    }
    /**
     * Retrieves a post.
     *
     * @param int $id The id of the post.
     * @return Response
     */
    public function retrieve($id)
    {
        if ($id === null) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // Get the post.
        $post = $this->Posts->get($id);

        // If the requested post does not exist.
        if (!$post) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // If post has been deleted.
        if ($post->deleted == 1) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        return $this->Response->success(
            $this->response,
            'retrieve_success',
            [
                'post' => $post
            ]
        );
    }
    /**
     * Deletes a post
     *
     * @param int $id The id of the post.
     * @return Response
     */
    public function delete($id)
    {
        if ($id === null) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }
        $post = $this->Posts->get($id);

        // If the requested post does not exist.
        if (!$post) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // If post has been deleted.
        if ($post->deleted == 1) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // Get the user id.
        $user_id = $this->Clients->getUserId($this->request);

        // If no user id retrieved.
        if (!$user_id) {
            return $this->Response->unauthorized(
                $this->response
            );
        }

        // Check if the user owns the post.
        if ($post->user_id != $user_id) {
            return $this->Response->forbidden(
                $this->response
            );
        }

        // Delete post.
        $post->deleted = 1;
        $post->deleted_at = date('Y-m-d H:i:s');

        return $this->Response->success(
            $this->response,
            'delete_success',
            [
                'id' => $id
            ]
        );
    }
    /**
     * Likes/unlikes a post.
     *
     * @param int $id The id of the post.
     * @return Response
     */
    public function like($id)
    {
        // Get post.
        $post = $this->Posts->find('all')
            ->where(
                [
                    'id' => $id
                ]
            )
            ->contain(
                [
                    'Likes'
                ]
        );
        $post = $post->first();

        // If empty.
        if (!$post) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // If deleted.
        if ($post->deleted == 1) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // Load user id. 
        $user_id = $this->Clients->getUserId($this->request);

        // If user_id is null.
        if ($user_id === null) {
            return $this->Response->unauthorized(
                $this->response
            );
        }
        // Iterate over the likes.
        foreach ($post->likes as $like) {
            if ($like->user_id == $user_id) {
                $like->deleted = 1;
                $like->deleted_at = date('Y-m-d H:i:s');
                if (!$this->Likes->save($like)) {
                    return $this->Response->internalError(
                        $this->response
                    );
                }
                return $this->Response->success(
                    $this->response,
                    'unliked',
                    [
                        "id" => $id
                    ]
                );
            }
        }
        $newLike = new \App\Model\Entity\Like();
        $newLike->user_id = $user_id;
        $newLike->post_id = $post->id;
        $newLike->created = date('Y-m-d H:i:s');
        if (!$this->Likes->save($newLike)) {
            return $this->Response->internalError(
                $this->response
            );
        }
        return $this->Response->success(
            $this->response,
            'liked',
            [
                "id" => $id
            ]
        );
    }
    /**
     * Directly retweets another post.
     *
     * @param int $id The post to retweet.
     * @return void
     */
    public function retweet($id)
    {
        // Receive the payload.
        $validator = new PostValidator();
        $errors = $validator->errors($this->request->getData());

        // If there are errors. Return success but with form_errors
        // status. Returning the previous payload and the errors.
        if ($errors) {
            return $this->Response->success(
                $this->response,
                'form_errors',
                [
                    'payload' => $this->request->getData(),
                    'errors' => $errors
                ]
            );
        }

        // Check post to retweet.
        $post = $this->Posts->get($id);

        // Post not found.
        if (empty($post)) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // Post soft deleted.
        if ($post->deleted == 1) {
            return $this->Response->failed(
                $this->response,
                'post_not_found'
            );
        }

        // Create new post.
        $post = new Post();

        // Get user id. 
        $user_id = $this->Clients->getUserId($this->request);

        if ($user_id === null) {
            return $this->Response->unauthorized(
                $this->response
            );
        }

        $post->title   = '@retweet';
        $post->body    = nl2br($this->request->getData('body'));
        $post->created = date('Y-m-d H:i:s');
        $post->post_id = $id;
        $post->user_id = $user_id;

        // Save the post.
        if (!$this->Posts->save($post)) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // Return the created post id so that client
        // can use it to immediately redirect to the Post.
        return $this->Response->success(
            $this->response,
            'save_success',
            [
                'id' => $post->id
            ]
        );
    }
}