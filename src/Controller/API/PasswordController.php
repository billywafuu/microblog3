<?php namespace App\Controller\Api;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class PasswordController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Response',
            [
                'className' => '\App\Controller\Component\API\ResponseComponent'
            ]
        );
        $this->loadComponent('Email');
        $this->Auth->allow(
            [
                'sendResetEmail'
            ]
        );
        $this->Users = TableRegistry::getTableLocator()->get('users');
        $this->Activations = TableRegistry::getTableLocator()->get('activations');
    }
    /**
     * Sends the password reset email to the user's email address.
     *
     * @return void
     */
    public function sendResetEmail()
    {

        // Get the payload.
        $payload = $this->request->getData();

        // Check the email address payload. 
        if (!isset($payload['email_address'])) {
            return $this->Response->failed(
                $this->response,
                'no_email_address'
            );
        }

        // The email.
        $result = $this->Email->sendForgotPassword(
            $payload['email_address']
        );

        // When the email was not found.
        if ($result == 3) {
            return $this->Response->success(
                $this->response,
                'email_not_found'
            );
        }

        // When a recent request has been sent, let the
        // client wait 5 minutes first.
        if ($result == 2) {
            return $this->Response->success(
                $this->response,
                'delay_required'
            );
        }

        // General error.
        if ($result == 0) {
            return $this->Response->internalError(
                $this->response,
                'error_occurred'
            );
        }

        // When the email was successfully sent.
        return $this->Response->success(
            $this->response,
            'email_sent'
        );
    }
}
