<?php namespace App\Controller\Api;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Validation\UserUpdateValidator;

class UsersController extends AppController
{
    public function initialize() 
    {
        parent::initialize();
        $this->loadComponent(
            'Response',
            [
                'className' => '\App\Controller\Component\API\ResponseComponent'
            ]
        );
        $this->loadComponent(
            'Authentication',
            [
                'className' => '\App\Controller\Component\API\AuthenticationComponent'
            ]
        );
        $this->loadComponent('Email');
        $this->loadComponent('Account');
        $this->Auth->allow(
            [
                'create',
                'activate',
                'update'
            ]
        );
        $this->Users = TableRegistry::getTableLocator()->get('users');
        $this->Activations = TableRegistry::getTableLocator()->get('activations');
    }
    /**
     * Receives payload of user details for registration.
     *
     * @return void
     */
    public function create()
    {
        // Get data from the request body.
        $registrationForm = $this->request->getData();

        // Empty check.
        if (empty($registrationForm)) {
            return $this->Response->forbidden(
                $this->response
            );
        }

        // Load the validator.
        $validator = new \App\Model\Validation\UserValidator();

        // Validate the form.
        $errors = $validator->errors($registrationForm);

        // Check if there are errors.
        if ($errors) {
            return $this->Response->success(
                $this->response,
                'form_errors',
                [
                    'payload' => $registrationForm,
                    'errors'  => $errors
                ]
            );
        }

        // Load the users table.
        $Users = TableRegistry::getTableLocator()->get('users');

        // Check if email address is available for use.
        if (!$Users->isEmailAvailable($registrationForm['email_address'])) {
            return $this->Response->success(
                $this->response,
                'email_unavailable'
            );
        }

        // Create the user.
        $user = $Users->create($registrationForm);

        // Send an email to the user. 
        if (!$this->Email->sendActivation($user)) {
            return $this->Response->internalError(
                $this->response,
                'email_send_failed'
            );
        }
        return $this->Response->success(
            $this->response,
            'register_success',
            [
                'user_id' => $user->id
            ]
        );
    }
    /**
     * Activates an account. This method requires account id
     * and the code to activate.
     *
     * @return void
     */
    public function activate()
    {

        // Gets the activation payload.
        $activation = $this->request->getData();

        // Check. The payload must consist of user_id and
        // the respective code.
        if (!isset($activation['user_id']) || !isset($activation['code'])) {
            return $this->Response->failed(
                $this->response
            );
        }

        // Get the user.
        $user = $this->Users->get($activation['user_id']);

        // If empty.
        if (empty($user)) {
            return $this->Response->success(
                $this->response,
                'no_user_found'
            );
        }

        // If already activated.
        if ($user->status == 1) {
            return $this->Response->success(
                $this->response,
                'already_activated'
            );
        }

        // Pull the activation record.
        $activation = $this->Activations->find()
            ->where(
                [
                    'user_id' => $user->id,
                    'code' => $activation['code']
                ]
            )->first();

        // Check if there is such activation.
        if (empty($activation)) {
            return $this->Response->success(
                $this->response,
                'invalid_activation'
            );
        }

        // If there is a matching activation, activate the account.
        $user->status = 1;
        if (!$this->Users->save($user)) {
            return $this->Response->internalError(
                $this->response,
                'save_failed'
            );
        }

        // Delete the activation.
        $this->Activations->delete($activation);

        // Delete all users with the same email_address and
        // not yet activated.
        $this->Users->deleteAll(
            [
                'email_address' => $user->email_address,
                'status' => 0
            ]
        );

        // Return success.
        return $this->Response->success(
            $this->response,
            'account_activated'
        );
    }

    /**
     * Accessed by PUT method. Updates the account
     * of the user.
     *
     * @param int $user_id
     * @return void
     */
    public function update($id)
    {
        // Get data from the request body.
        $payload = $this->request->getData();

        // Empty check.
        if (empty($payload)) {
            return $this->Response->forbidden(
                $this->response
            );
        }

        // Load the validator.
        $validator = new UserUpdateValidator();

        // Validate the payload.
        $errors = $validator->errors($payload);
 
        // Check if there are errors.
        if ($errors) {
            return $this->Response->success(
                $this->response,
                'form_errors',
                [
                    'payload' => $payload,
                    'errors'  => $errors
                ]
            );
        }

        // Get user id.
        $user_id = $this->Authentication->getUserId(
            $this->request->getHeaderLine('Authorization')
        );

        // Check if the user id is null.
        if ($user_id === null) {
            return $this->Response->unauthorized(
                $this->response
            );
        }

        // Compare the accessing user_id. 
        if ($id != $user_id) {
            return $this->Response->unauthorized(
                $this->response
            );
        }

        // Get the user.
        $user = $this->Users->get($user_id);

        // If empty.
        if (empty($user)) {
            return $this->Response->failed(
                $this->response,
                'user_not_found'
            );
        }

        // Try to update the user account.
        $result = $this->Account->update(
            $user_id,
            $payload
        );

        // It means that the initial update was a failure.
        // Return bad request.
        if ($result != 1) {
            return $this->Response->failed(
                $this->response
            );
        }
 
        // Get the email address.
        $email_address = $payload['email_address'];

        // if its the same, no need to initiate email
        // address change.
        if ($email_address == $user->email_address) {
            return $this->Response->success(
                $this->response,
                'save_success'
            );
        }

        // If the user initiated an email change, proceed to
        // lock the account and send an activation email again.
        $result = $this->Account->updateEmailAddress(
            $user_id,
            $email_address
        );

        // When the email address is not available for taking.
        if ($result == 2) {
            return $this->Response->success(
                $this->response,
                'email_unavailable'
            );
        }

        // Internal error
        if ($result == 0) {
            return $this->Response->internalError(
                $this->response
            );
        }

        // Success.
        return $this->Response->success(
            $this->response,
            'email_changed'
        );
    }
}
