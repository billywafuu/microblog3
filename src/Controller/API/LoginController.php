<?php namespace App\Controller\Api;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use App\Controller\AppController;
use App\Model\Entity\Client;

class LoginController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Response',
            [
                'className' => '\App\Controller\Component\API\ResponseComponent'
            ]
        );
        $this->loadComponent(
            'Authentication',
            [
                'className' => '\App\Controller\Component\API\AuthenticationComponent'
            ]
        );
        $this->loadComponent('Rangen');
        $this->Auth->allow(
            [
                'index',
                'verify',
                'destroy'
            ]
        );
        $this->Users = TableRegistry::getTableLocator()->get('users');
        $this->Clients = TableRegistry::getTableLocator()->get('clients');
    }
    /**
     * Login Index
     * 
     * Receives email address and password combination
     * to log the user in.
     *
     * @return Cake\Http\Request Returns a request with a body containing
     * the response.
     */
    public function index()
    {
        // Get data from the request body.
        $login = $this->request->getData();

        // Empty check.
        if (empty($login)) {
            return $this->Response->unauthorized(
                $this->response
            );
        }

        // Check for payload. If either payload is missing, return failed.
        if (!isset($login['email_address']) || !isset($login['password'])) {
            return $this->Response->failed(
                $this->response
            );
        }

        // Get the user record from the users table.
        $user = $this->Users->find('all')
            ->where(
                [
                    'Users.email_address' => $login['email_address'],
                    'Users.status' => 1
                ]
            )->first();

        // If no user found.
        if (empty($user)) {
            return $this->Response->failed(
                $this->response
            );
        }

        // Verify password.
        if (!password_verify($login['password'], $user['password'])) {
            return $this->Response->unauthorized(
                $this->response,
                'login_failed'
            );
        }

        // If still needs activation.
        if ($user['status'] == 0) {
            return $this->Response->forbidden(
                $this->response,
                'activation_required'
            );
        }

        // Generate access_token for the client. 
        $access_token = $this->Rangen->generate(32);

        // Create a new client.
        $client = $this->Authentication->createClient(
            $user['id'],
            $access_token,
            $this->request
        );

        // Save the client.
        if (!$this->Clients->save($client)) {
            return $this->Response->internalError(
                $this->response,
                'save_failed'
            );
        }

        // Create an Authorization token out of the user id and the access_token.
        $Authorization = $this->Authentication->createAuthorization($user['id'], $access_token);

        return $this->Response->success(
            $this->response,
            'login_success',
            [
                'Authorization' => $Authorization
            ]
        );
    }
    /**
     * Reads the Api-Auth-Object header to
     * check against the client record.
     *
     * @return Cake\Http\Request Returns a request with a body containing
     * the response.
     */
    public function verify()
    {
        // Reads the Authorization header.
        if (!$this->request->hasHeader('Authorization')) {
            return $this->Response->unauthorized(
                $this->response
            );
        }
        // Get the Authorization header.
        $Authorization = $this->request->getHeaderLine('Authorization');
        // Verify.
        if (!$this->Clients->verify($Authorization)) {
            return $this->Response->unauthorized(
                $this->response,
                'credentials_invalid'
            );
        }
        return $this->Response->success(
            $this->response,
            'credentials_valid'
        );
    }
    /**
     * Destroys a login credential.
     *
     * @return Cake\Http\Request Returns a request with a body containing
     * the response.
     */
    public function destroy()
    {
        // Reads the Authorization header.
        if (!$this->request->hasHeader('Authorization')) {
            return $this->Response->success(
                $this->response
            );
        }

        // Get the Authorization header.
        $Authorization = $this->request->getHeaderLine('Authorization');

        // Destroy the server Authorization counterpart.
        if (!$this->Clients->logout($Authorization)) {
            return $this->Response->unauthorized(
                $this->response,
                'logout_fail'
            );
        }

        // When the log out succeeds.
        return $this->Response->success(
            $this->response,
            'logout_success'
        );
    }
}