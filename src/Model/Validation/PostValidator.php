<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class PostValidator extends Validator
{
    public function __construct()
    {
        parent::__construct();
        $this->requirePresence('title')
            ->allowEmptyString('title')
            ->add(
                'title',
                [
                'length' => [
                    'rule' => ['maxLength', 140],
                    'message' => 'The title should not exceed 140 characters long.',
                ]
                ]
            );

        $this->requirePresence('body')
            ->allowEmptyString('body')
            ->add(
                'body',
                [
                'length' => [
                    'rule' => ['maxLength', 140],
                    'message' => 'The body should not exceed 140 characters long.',
                ]
                ]
            );
    }
}
