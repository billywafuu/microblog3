<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class UserUpdateValidator extends Validator
{
    public function __construct()
    {
        parent::__construct();
        $this->requirePresence('first_name')
        ->notEmptyString('first_name', 'Please fill out this field.')
        ->add('first_name', ['invalidFormat' => [
            'rule' => array('custom', '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u '),
            'message' => 'Please fill out the field properly.'
        ]]);

        $this->allowEmptyString('middle_name')
        ->add('middle_name', ['invalidFormat' => [
            'rule' => array('custom', '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u '),
            'message' => 'Please fill out the field properly.'
        ]]);

        $this->requirePresence('last_name')
        ->notEmptyString('last_name', 'Please fill out this field.')
        ->add('last_name', ['invalidFormat' => [
            'rule' => array('custom', '/[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+/u '),
            'message' => 'Please fill out the field properly.'
        ]]);

        $this->email('email_address', 'The email address format is invalid.');
        $this->requirePresence('birthdate')
        ->date('birthdate');
    }
}
