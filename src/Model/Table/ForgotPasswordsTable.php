<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class ForgotPasswordsTable extends Table
{
    /**
     * Custom finder to find the last request of forgot password
     * of a certain user.
     *
     * @param Query $query
     * @param array $user_id The user id of the user.
     * @return null|App\Model\Entity\ForgotPassword
     */
    public function findByLastRequest(Query $query, array $user_id)
    {
        return $query->where(
            [
                'user_id' => $user_id['user_id'],
            ]
        )->order(
            [
                'created DESC'
            ]
        )
        ->first();
    }
}
