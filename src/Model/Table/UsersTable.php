<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use App\Model\Entity\User;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addAssociations(
            [
            'hasMany' => [
                'Posts' => [
                    'className' => 'App\Model\Table\PostsTable',
                    'foreignKey' => 'user_id'
                ]
            ]
            ]
        );
    }
    /**
     * Custom finder to find user entity by email address.
     */
    public function findByEmailAddress(Query $query, array $email_address)
    {
        return $query->where(
            [
                'email_address' => $email_address['email_address'],
                'status' => 1
            ]
        )->first();
    }
    /**
     * Creates a user and save it in the database.
     * During this process, password is already encrypted.
     *
     * @param Array $user
     * @return bool|int Returns created object and false when failed.
     */
    public function create(array $user)
    {
        try {
            $n = new User($user);
            $n->password = password_hash(
                $n->password,
                PASSWORD_BCRYPT
            );
            $n->created = date('Y-m-d H:i:s');
            $this->save($n);
            return $n;
        } catch (\Exception $e) {
            debug($e);
            return false;
        }
    }
    /**
     * Checks if the email address is available for use.
     *
     * @param string $email_address
     * @return boolean
     */
    public function isEmailAvailable($email_address)
    {
        $query = $this->find(
            'byEmailAddress',
            ['email_address' => $email_address]
        );

        if (empty($query)) {
            return true;
        }
        return false;
    }
}
