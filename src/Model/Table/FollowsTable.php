<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class FollowsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addAssociations(
            [
            'belongsTo' => [
                    'User' => [
                        'className' => 'App\Model\Table\UsersTable',
                        'foreignKey' => 'user_id'
                    ],
                    'Following' => [
                        'className' => 'App\Model\Table\UsersTable',
                        'foreignKey' => 'following_id'
                    ]
                ],
            'hasMany' => [
                'Posts' => [
                    'className' => 'App\Model\Table\PostsTable',
                    'foreignKey' => 'user_id',
                    'bindingKey' => 'following_id',
                    'conditions' => [
                        'Posts.deleted' => 0
                    ]
                ]
            ]
            ]
        );
    }
}
