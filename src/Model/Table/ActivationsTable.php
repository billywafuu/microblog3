<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use App\Model\Entity\Activation;

class ActivationsTable extends Table
{
    /**
     * Creates a new activation record that
     * activates account when a link with the
     * right combination was executed.
     *
     * @param int $user_id The id of the user that will be activated.
     * @param string $id The activation id.
     * @param string $activation_token The activation token of this activation.
     * @return void
     */
    public function create($user_id, $id, $activation_token)
    {
        $activation  = new Activation(
            [
                'id' => $id,
                'activation_token' => $activation_token,
                'user_id' => $user->id
            ]
        );
        if (!$this->save($activation)) {
            return false;
        }
        return true;
    }
}
