<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use App\Model\Entity\Client;

class ClientsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addAssociations(
            [
                'belongsTo' => [
                    'Owner' => [
                        'className' => 'App\Model\Table\UsersTable',
                        'foreignKey' => 'user_id'
                    ]
                ]
            ]
        );
    }
    /**
     * Verifies the Authorization token.
     *
     * @param base64 $Authorization
     * @return bool Returns true if valid.
     */
    public function verify($Authorization)
    {
        // Decode base64.
        $Authorization = base64_decode($Authorization, true);
        if (!$Authorization) {
            return false;
        }

        // Explode the line into <user_id>:<token>
        $Authorization = explode(':', $Authorization);

        // Check if there is a successful explode.
        if (!isset($Authorization[0]) || !isset($Authorization[1])) {
            return false;
        }

        // Verify the Authorization.
        $client = $this->find('all')
            ->where(
                [
                    'user_id' => $Authorization[0],
                    'access_token' => $Authorization[1]
                ]
            )->first();
        if (empty($client)) {
            return false;
        }

        // If client expired.
        $date = new \DateTime($client->date_expiry);
        $now  = new \DateTime();
        if ($date < $now) {
            $this->delete($client);
            return false;
        }
        return true;
    }
    /**
     * Logs out account by destroying the client record
     * from the database. The client record that will be destroyed 
     * will depend from the Authorization header sent.
     *
     * @param base64 $Authorization
     * @return void
     */
    public function logout($Authorization) {

        // Verify
        if (!$this->verify($Authorization)) {
            return false;
        }

        // Decode base64.
        $Authorization = base64_decode($Authorization, true);

        // Explode the line into <user_id>:<token>
        $Authorization = explode(':', $Authorization);

        // Get client.
        $client = $this->find('all')
            ->where(
                [
                    'user_id' => $Authorization[0],
                    'access_token' => $Authorization[1]
                ]
            )->first();

        // Delete the client.
        $this->delete($client);
        return true;
    }

    /**
     * Gets the user id from the request.
     *
     * @param Cake\HTTP\Request $request
     * @return int|null Returns the id. Null if no id.
     */
    public function getUserId($request)
    {
        // If no request.
        if (!$request) {
            return null;
        }
        if (!$request->hasHeader('Authorization')) {
            return null;
        }
        $Authorization = $request->getHeaderLine('Authorization');

        if (!$this->verify($Authorization)) {
            return null;
        }
        // Decode base64.
        $Authorization = base64_decode($Authorization, true);

        // Explode the line into <user_id>:<token>
        $Authorization = explode(':', $Authorization);

        // Return the user id.
        return $Authorization[0];
    }
}
