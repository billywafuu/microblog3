<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class CommentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addAssociations(
            [
            'belongsTo' => [
                'Owner' => [
                    'className' => 'App\Model\Table\UsersTable',
                    'foreignKey' => 'user_id'
                ],
                'Post' => [
                    'className' => 'App\Model\Table\PostsTable',
                    'foreignKey' => 'post_id',
                    'conditions' => [
                        'Posts.deleted' => 0
                    ]
                ]
            ]
            ]
        );
    }
}
