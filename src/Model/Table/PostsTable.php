<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class PostsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addAssociations(
            [
            'belongsTo' => [
                'Owner' => [
                    'className' => 'App\Model\Table\UsersTable',
                    'foreignKey' => 'user_id'
                ],
                'Retweet' => [
                    'className' => 'App\Model\Table\PostsTable',
                    'foreignKey' => 'post_id',
                    'conditions' => [
                        'Posts.deleted' => 0
                    ]
                ]
            ],
            'hasMany' => [
                'Likes' => [
                    'className' => 'App\Model\Table\LikesTable',
                    'foreignKey' => 'post_id',
                    'conditions' => [
                        'Likes.deleted' => 0
                    ]
                ]
            ]
            ]
        );
    }
}
