<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Client Entity
 *
 * A model representation of a Client that contains
 * client related methods.
 */
class Client extends Entity
{
    //
}
