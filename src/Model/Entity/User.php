<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * User Entity
 *
 * A model representation of a User that contains
 * user related methods.
 */
class User extends Entity
{
    /**
     * Gets the full name of the user.
     *
     * @return string The full name of the user.
     */
    protected function _getFullName()
    {
        return $this->first_name . '  ' . $this->last_name;
    }
    /**
     * Get the followers of this user.
     *
     * @return Entity|null An array of user entities.
     */
    public function getFollowers($search = "")
    {
        $db = TableRegistry::getTableLocator()->get('Follows');
        if ($search != "") {
            return $db->find('all')
            ->where(
                [
                    "OR" => [
                        "first_name LIKE '%$search%'",
                        "middle_name LIKE '%$search%'",
                        "last_name LIKE '%$search%'",
                        "CONCAT(first_name, ' ', last_name) LIKE '%$search%'",
                        "CONCAT(last_name, ' ', first_name) LIKE '%$search%'",
                        "email_address LIKE '%$search%'"
                    ],
                    'User.deleted' => 0,
                    'following_id' => $this->id,
                    'date_unfollowed IS NULL',
                    'user_id != ' . $this->id
                ]
            )->contain(
                [
                    'User'
                ]
            )
            ->all();
        }
        return $db->find('all')
            ->where(
                [
                    'following_id' => $this->id,
                    'date_unfollowed IS NULL',
                    'user_id != ' . $this->id
                ]
            )->contain(
                [
                    'User'
                ]
            )
            ->all();
    }
    /**
     * Get the users that this user is following.
     *
     * @return Entity|null An array of user entities.
     */
    public function getFollowings($search = "")
    {
        $db = TableRegistry::getTableLocator()->get('Follows');
        if ($search != "") {
            return $db->find()
            ->where(
                [
                    "OR" => [
                        "first_name LIKE '%$search%'",
                        "middle_name LIKE '%$search%'",
                        "last_name LIKE '%$search%'",
                        "CONCAT(first_name, ' ', last_name) LIKE '%$search%'",
                        "CONCAT(last_name, ' ', first_name) LIKE '%$search%'",
                        "email_address LIKE '%$search%'"
                    ],
                    'Following.deleted' => 0,
                    'user_id' => $this->id,
                    'Following.following_id != ' . $this->id,
                    'date_unfollowed IS NULL'
                ]
            )->contain(
                [
                    'Following'
                ]
            )
            ->all();
        }
        return $db->find('all')
            ->where(
                [
                    'user_id' => $this->id,
                    'date_unfollowed IS NULL',
                    'following_id != ' . $this->id
                ]
            )->contain(
                [
                    'Following'
                ]
            )
            ->all();
    }
    /**
     * Gets all the post of this user.
     *
     * @return array An array of posts.
     */
    public function getPosts()
    {
        // TODO
    }
    /**
     * Make this user follow a user.
     *
     * @param int $id
     * @return bool Returns true when success otherwise false.
     */
    public function setFollow($id)
    {
        try {
            $follows = TableRegistry::getTableLocator()->get('Follows');
            $exists = $follows->find()->where(
                [
                    'following_id' => $id,
                    'user_id' => $this->id
                ]
            )->first();

            if ($exists) {
                return 1;
            }

            $newFollow = new Follow();
            $newFollow->following_id = $id;
            $newFollow->user_id = $this->id;
            if (!$follows->save($newFollow)) {
                return 0;
            }
            return 1;
        } catch (\Exception $e) {
            debug($e);
            return 0;
        }
    }
    /**
     * Checks if this user is following the other user.
     *
     * @param int $user_id The user id of the other user to check.
     * @return boolean
     */
    public function isFollowing($user_id)
    {
        $Follows = TableRegistry::getTableLocator()->get('Follows');
        $follow = $Follows->find()
            ->where('user_id', $this->id)
            ->where('following_id', $user_id)
            ->where('date_unfollowed', null)
            ->first();
        if (empty($follow)) {
            return false;
        }
        return true;
    }
}
