<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use App\Middleware\CsFilterMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
    ]));
    $routes->registerMiddleware('csfilter', new CsFilterMiddleware());
    /*
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered through `Application::routes()` with `registerMiddleware()`
     */
    //$routes->applyMiddleware('csrf');

    /*
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
    $routes->connect('/', ['controller' => 'Microblog', 'action' => 'index', 'home']);
    $routes->connect('/login', ['controller' => 'Microblog', 'action' => 'login']);
    $routes->connect('/profile', ['controller' => 'Users', 'action' => 'profile']);
    $routes->connect('/logout', ['controller' => 'Microblog', 'action' => 'logout']);
    $routes->connect('/registration', ['controller' => 'Microblog', 'action' => 'registration']);
    $routes->connect('/activation', ['controller' => 'Microblog', 'action' => 'activation']);
    $routes->connect('/lang', ['controller' => 'Microblog', 'action' => 'lang']);
    $routes->connect('/search', ['controller' => 'Microblog', 'action' => 'search']);
    $routes->connect('/forgotpassword', ['controller' => 'Microblog', 'action' => 'forgotpassword']);
    $routes->connect('/emailtester', ['controller' => 'Microblog', 'action' => 'emailtester']);
    //-----------------------------------------------------------------------------------------
    $routes->connect(
        '/activation/:id/:activation_token',
        ['controller' => 'Microblog', 'action' => 'activation']
    )
    ->setPass(['id', 'activation_token'])
    ->setPatterns([
        'id' => '[a-zA-Z0-9]+',
        'activation_token' => '[a-zA-Z0-9]+',
    ]);
    //-----------------------------------------------------------------------------------------
    $routes->connect(
        '/resetpassword/:id/:reset_token',
        ['controller' => 'Microblog', 'action' => 'resetpassword']
    )
    ->setPass(['id', 'reset_token'])
    ->setPatterns([
        'id' => '[a-zA-Z0-9]+',
        'reset_token' => '[a-zA-Z0-9]+',
    ]);
    //-----------------------------------------------------------------------------------------
    $routes->connect(
        '/users/view/:id/network',
        ['controller' => 'Users', 'action' => 'network']
    )
    ->setPass(
        [
            'id'
        ]
    )
    ->setPatterns(
        [
            'id' => '[0-9]+',
        ]
    );
    //---------------------------------API Routes----------------------------------------------
    Router::prefix('api', function (RouteBuilder $routes) {
        $routes->applyMiddleware('csfilter');
        $routes->post(
            '/verify',
            ['controller' => 'Login', 'action' => 'verify']
        );
        $routes->post(
            '/login',
            ['controller' => 'Login', 'action' => 'index']
        );
        $routes->post(
            '/logout',
            ['controller' => 'Login', 'action' => 'destroy']
        );
        $routes->post(
            '/users',
            ['controller' => 'Users', 'action' => 'create']
        );
        $routes->put(
            '/users/:id',
            ['controller' => 'Users', 'action' => 'update']
        )->setPass(['id'])
        ->setPatterns(['id' => '[0-9]+']);
        $routes->post(
            '/activate',
            ['controller' => 'Users', 'action' => 'activate']
        );
        $routes->post(
            '/forgotpassword',
            ['controller' => 'Password', 'action' => 'sendResetEmail']
        );

        //=============================================================
        // Posts
        //=============================================================
        $routes->post(
            '/posts',
            ['controller' => 'Posts', 'action' => 'create']
        );
        $routes->post(
            '/posts/:id',
            ['controller' => 'Posts', 'action' => 'update']
        )->setPass(['id'])
        ->setPatterns(['id' => '[0-9]+']);

        $routes->get(
            '/posts/:id',
            ['controller' => 'Posts', 'action' => 'retrieve']
        )->setPass(['id'])
        ->setPatterns(['id' => '[0-9]+']);

        $routes->delete(
            '/posts/:id',
            ['controller' => 'Posts', 'action' => 'delete']
        )->setPass(['id'])
        ->setPatterns(['id' => '[0-9]+']);
        $routes->post(
            '/posts/:id/like',
            ['controller' => 'Posts', 'action' => 'like']
        )->setPass(['id'])
        ->setPatterns(['id' => '[0-9]+']);
        $routes->post(
            '/posts/:id/retweet',
            ['controller' => 'Posts', 'action' => 'retweet']
        )->setPass(['id'])
        ->setPatterns(['id' => '[0-9]+']);
        $routes->fallbacks(DashedRoute::class);
    });
    //-----------------------------------------------------------------------------------------
    /*
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    //$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /*
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *
     * ```
     * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
     * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
     * ```
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
