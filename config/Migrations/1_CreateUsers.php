<?php
use Migrations\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn(
            'first_name',
            'text',
            [
                'default' => null,
                'limit' => 255,
                'null' => false
            ]
        );
        $table->addColumn(
            'middle_name',
            'text',
            [
            'default' => null,
            'limit' => 255,
            'null' => false
            ]
        );
        $table->addColumn(
            'last_name',
            'text',
            [
                'default' => null,
                'limit' => 255,
                'null' => false
            ]
        );
        $table->addColumn(
            'email_address',
            'text',
            [
                'default' => null,
                'limit' => 255,
                'null' => false
            ]
        );
        $table->addColumn(
            'password',
            'text',
            [
                'default' => null,
                'limit' => 255,
                'null' => false
            ]
        );
        $table->addColumn(
            'birthdate',
            'date',
            [
                'default' => null,
                'null' => false
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ]
        );
        $table->addColumn(
            'deleted',
            'integer',
            [
                'default' => 0,
                'limit' => 11,
                'null' => false,
                'comment' => '0 - Not deleted, 1 - Deleted'
            ]
        );
        $table->addColumn(
            'deleted_at',
            'datetime',
            [
                'default' => null,
                'null' => true,
                'comment' => '0 - Not deleted, 1 - Deleted'
            ]
        );
        $table->addColumn(
            'status',
            'integer',
            [
                'default' => 0,
                'limit' => 1,
                'null' => false,
                'comment' => '0 - Inactive, 1 - Active'
            ]
        );
        $table->addColumn(
            'image_file_type',
            'text',
            [
                'default' => null,
                'limit' => 4,
                'null' => true
            ]
        );
        $table->create();
    }
}