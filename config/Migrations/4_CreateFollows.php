<?php
use Migrations\AbstractMigration;

class CreateFollows extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('follows');
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'following_id',
            'integer',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'date_followed',
            'datetime',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ]
        );
        $table->addColumn(
            'date_unfollowed',
            'datetime',
            [
                'default' => null,
                'null' => true
            ]
        );
        $table->create();
    }
}