<?php
use Migrations\AbstractMigration;

class CreateForgotPasswords extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('forgot_passwords');
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'reset_token',
            'text',
            [
                'default' => null,
                'limit' => 16,
                'null' => false
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false
            ]
        );
        $table->create();
    }
}