<?php
use Migrations\AbstractMigration;

class CreateClients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('clients');
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'access_token',
            'text',
            [
                'default' => null,
                'limit' => 32,
                'null' => false
            ]
        );
        $table->addColumn(
            'user_agent',
            'text',
            [
                'default' => null,
                'limit' => 255,
                'null' => false
            ]
        );
        $table->addColumn(
            'ip_address',
            'text',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'date_expiry',
            'datetime',
            [
                'default' => null,
                'null' => false
            ]
        );
        $table->create();
    }
}