<?php
use Migrations\AbstractMigration;

class CreateActivations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(
            'activations'
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'activation_token',
            'text',
            [
                'default' => null,
                'limit' => 16,
                'null' => false
            ]
        );
        $table->addColumn(
            'code',
            'text',
            [
                'default' => null,
                'limit' => 6,
                'null' => false
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ]
        );
        $table->create();

    }
}
