<?php
use Migrations\AbstractMigration;

class CreatePosts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('posts');
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 20,
                'null' => false
            ]
        );
        $table->addColumn(
            'body',
            'text',
            [
                'default' => null,
                'limit' => 140,
                'null' => false
            ]
        );
        $table->addColumn(
            'title',
            'text',
            [
                'default' => null,
                'limit' => 140,
                'null' => false
            ]
        );
        $table->addColumn(
            'created',
            'datetime',
            [
                'default' => null,
                'null' => false
            ]
        );
        $table->addColumn(
            'modified',
            'datetime',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false
            ]
        );
        $table->addColumn(
            'deleted',
            'integer',
            [
                'default' => 0,
                'limit' => 11,
                'null' => false,
                'comment' => '0 - Not deleted, 1 - Deleted'
            ]
        );
        $table->addColumn(
            'deleted_at',
            'datetime',
            [
                'default' => null,
                'null' => true,
                'comment' => '0 - Not deleted, 1 - Deleted'
            ]
        );
        $table->addColumn(
            'status',
            'integer',
            [
                'default' => 0,
                'limit' => 1,
                'null' => false,
                'comment' => '0 - Inactive, 1 - Active'
            ]
        );
        $table->addColumn(
            'image_file_type',
            'text',
            [
                'default' => null,
                'limit' => 4,
                'null' => true
            ]
        );
        $table->addColumn(
            'post_id',
            'integer',
            [
                'default' => null,
                'limit' => 11,
                'null' => true
            ]
        );
        $table->create();
    }
}